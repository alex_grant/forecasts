scipy >= 1.6.0
kombine
emcee
# Note: you need to install numpy first, because the py3nj package does not
# actually specify it as a build dependency...also we require numpy >= 1.20.0
py3nj
matplotlib
gwsurrogate
h5py
scikit-learn
lalsuite
astropy
ijson
