GWForecasts
===========

GWForecasts is a Python library/software package for predicting the ability for gravitational wave detectors to measure the gravitational wave memory effect (although it could easily be extended to other types of signals).
For examples as to how to use this package, see the `examples` directory.
The `scripts` directory contains scripts that are useful for generating and manipulating output, and `gwforecasts` provides the main library of code.

This package was used to calculate the results in the following paper:

Alexander. M. Grant, David A. Nichols, *Outlook for detecting the gravitational wave displacement and spin memory effects with current and future gravitational wave detectors*, [Phys. Rev. D **107**, 064056 (2023)](https://doi.org/10.1103/PhysRevD.107.064056), [arXiv:2210.16266](https://arxiv.org/abs/2210.16266)

If you use this package as part of your own work, please cite the above paper.

To install the required dependencies, you can simply run

`
pip install -r requirements.txt
`

from the top level directory.  If you do not already have a sufficiently up-to-date version of numpy installed, you will need to run

`
pip install "numpy>=1.20.0"
`

first.  This is because one of the required dependencies for this package (py3nj) does not explicitly list numpy as a dependency, and so it will attempt to be installed beforehand.