import math

import numpy as np

from .detector import Detector

YEAR_SI = 3.154e7

class Network:

    def __init__(self, min_detectors=1, network_config=None):
        self.detectors = {}
        self.min_detectors = min_detectors
        if network_config is not None:
            for key in network_config:
                self.add_detectors(key, **network_config[key])

    def add_detectors(self, key, *, duty_cycle=1.,
                     time_min=-math.inf, time_max=math.inf, detectors):
        self.detectors[key] = ((duty_cycle,
                                np.random.default_rng().integers(1e5)),
                               (time_min, time_max),
                               [Detector(**params) for params in detectors])

    def load_antenna(self, ra, dec, pol, time):
        return {key: [det.antenna_pattern(ra, dec, pol, time)
                      for det in self.detectors[key][2]]
                for key in self.detectors}

    def snr2(self, freqs, htilde, f_0, f_1, antenna_patterns, time):
        # Note: time is in years!
        snr2 = 0.
        n = 0

        for key in self.detectors:
            duty_cycle, base_seed = self.detectors[key][0]
            time_min, time_max = self.detectors[key][1]
            detectors = self.detectors[key][2]
            if time_min <= time <= time_max:
                rand = np.random.default_rng(seed=(math.floor(time*YEAR_SI)
                                                   + base_seed))

                if rand.uniform() < duty_cycle:
                    n += 1
                    for det, F in zip(detectors, antenna_patterns[key]):
                        snr2 += det.snr2(freqs, htilde, f_0, f_1, *F)

        if n >= self.min_detectors:
            return snr2
        else:
            return 0.
