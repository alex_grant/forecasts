import numpy as np

from .base import Noise

class DataPSD(Noise):

    def __init__(self, path, freq_func=None, psd_func=(lambda f, n: n**2),
                 usecols=(0, 1)):
        freqs, psd = np.loadtxt(path, unpack=True, usecols=usecols)

        if freq_func is not None:
            freqs = freq_func(freqs, psd)

        if psd_func is not None:
            psd = psd_func(freqs, psd)

        self.freqs, self.psd = freqs, psd

    def __call__(self, freqs):
        return np.interp(freqs, self.freqs, self.psd)
