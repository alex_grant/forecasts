from .base import Antenna

class NullAntenna(Antenna):

    def __init__(self, nonzero='plus'):
        self.nonzero = nonzero

    def __call__(self, ra, dec, pol, time):
        zero, one = ((0, 1) if np.ndim(ra) == 0 else
                     (np.zeros_like(ra), np.ones_like(ra)))

        if self.nonzero == 'plus':
            return one, zero
        if self.nonzero == 'cross':
            return zero, one
