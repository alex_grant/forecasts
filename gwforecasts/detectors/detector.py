from pathlib import Path
import json
import math

import numpy as np

from ..utils import integration

from . import null
from . import data
from . import ligo

class Detector:

    def __init__(self, detector=None, antenna_pattern=None, noise_psd=None,
                 antenna_kwargs={}, noise_kwargs={}):

        if detector is not None:
            if detector == 'LIGO':
                self.noise_psd = ligo.Ajith2011PSD()
                self.antenna_pattern = ligo.LALAntenna(**antenna_kwargs)
            else:
                raise ValueError('Unsupported detector:', detector)

        if antenna_pattern is not None:
            if antenna_pattern == 'Null':
                self.antenna_pattern = null.NullAntenna(**antenna_kwargs)
            elif antenna_pattern == 'LAL':
                self.antenna_pattern = ligo.LALAntenna(**antenna_kwargs)
            elif antenna_pattern[:2] == 'CE':
                length = antenna_kwargs.pop('length', 40)
                antenna_kwargs = {'detector': 'Custom',
                                  'name': antenna_pattern,
                                  'vertexElevation': 0.,
                                  'xArmAltitudeRadians': 0.,
                                  'yArmAltitudeRadians': 0.,
                                  'xArmMidpoint': length*500,
                                  'yArmMidpoint': length*500}

                if antenna_pattern[2:] == '1':
                    lat, long, theta_XE = 47.4, 8.5, 11
                elif antenna_pattern[2:] == '2':
                    lat, long, theta_XE = -31.5, 118, -58
                elif antenna_pattern[2:] == '3':
                    lat, long, theta_XE = 40.8, -113.8, -30
                else:
                    raise ValueError('Unsupported antenna pattern:',
                                     antenna_pattern)

                antenna_kwargs.update({'vertexLongitudeRadians':
                                       math.radians(long),
                                       'vertexLatitudeRadians':
                                       math.radians(lat),
                                       'xArmAzimuthRadians':
                                       math.pi/2 - math.radians(theta_XE),
                                       'yArmAzimuthRadians':
                                       -math.radians(theta_XE)})

                self.antenna_pattern = ligo.LALAntenna(**antenna_kwargs)
            else:
                raise ValueError('Unsupported antenna pattern:',
                                 antenna_pattern)
        elif detector is None:
            raise ValueError('Need to specify antenna pattern')

        if noise_psd is not None:
            psd_path = None
            if noise_psd == 'Ajith2011':
                self.noise_psd = ligo.Ajith2011PSD()
            elif noise_psd == 'LIGO-T1800044-v5':
                psd_path = Path(__file__).parent/'psds'/'LIGO'/'T1800044-v5'
                psd_path = psd_path/'aLIGODesign.txt'
            elif noise_psd == 'LIGO-T2000012-v2':
                psd_path = Path(__file__).parent/'psds'/'LIGO'/'T2000012-v2'
                detector = noise_kwargs.pop('detector')
                run = noise_kwargs.pop('run')

                if detector == 'KAGRA':
                    if run == 'O4':
                        sensitivity = noise_kwargs.pop('sensitivity')
                        if sensitivity not in ('low', 'high', 'top'):
                            raise ValueError('Invalid sensitivity',
                                             sensitivity,
                                             'for detector KAGRA')

                        if sensitivity == 'low':
                            psd_path = psd_path/'kagra_3Mpc.txt'
                        elif sensitivity == 'high':
                            psd_path = psd_path/'kagra_10Mpc.txt'
                        elif sensitivity == 'top':
                            psd_path = psd_path/'kagra_25Mpc.txt'
                    elif run == 'O5':
                        psd_path = psd_path/'kagra_80Mpc.txt'
                    elif run == '128Mpc':
                        psd_path = psd_path/'kagra_128Mpc.txt'
                    else:
                        raise ValueError('Invalid run', run,
                                         'for detector KAGRA')
                elif detector == 'LIGO':
                    if run == 'O3':
                        location = noise_kwargs.pop('location')
                        if location not in ('H', 'L'):
                            raise ValueError('Invalid location', location,
                                             'for detector LIGO')
                        psd_path = psd_path/('aligo_O3actual_' + location
                                             + '1.txt')
                    elif run == 'O4':
                        psd_path = psd_path/'aligo_O4high.txt'
                    elif run == 'O5':
                        psd_path = psd_path/'AplusDesign.txt'
                    else:
                        raise ValueError('Invalid run', run,
                                         'for detector LIGO')
                elif detector == 'Virgo':
                    if run == 'O3':
                        psd_path = psd_path/'avirgo_O3actual.txt'
                    elif run == 'O4':
                        psd_path = psd_path/'avirgo_O4high_NEW.txt'
                    elif run == 'O5':
                        sensitivity = noise_kwargs.pop('sensitivity')
                        if sensitivity not in ('low', 'high'):
                            raise ValueError('Invalid sensitivity',
                                             sensitivity,
                                             'for detector Virgo')
                        # Note that this is *opposite* of what the documentation
                        # says (the comments I put in the file are correct)
                        psd_path = psd_path/('avirgo_O5' + sensitivity
                                             + '_NEW.txt')
                    else:
                        raise ValueError('Invalid run', run,
                                         'for detector Virgo')
                else:
                    raise ValueError('Invalid detector', detector)

            elif noise_psd == 'LIGO-T1500293-v13':
                psd_path = Path(__file__).parent/'psds'/'LIGO'/'T1500293-v13'
                psd_path = psd_path/(noise_kwargs.pop('spec') + '.txt')
            elif noise_psd == 'CE-T2000017-v5':
                psd_path = Path(__file__).parent/'psds'/'CE'/'T2000017-v5'
                length = noise_kwargs.pop('length', 40)
                mode = noise_kwargs.pop('mode', 'default')

                if length == 40 and mode == 'default':
                    psd_path = psd_path/'cosmic_explorer.txt'
                elif length == 40 and mode == 'low-frequency':
                    psd_path = psd_path/'cosmic_explorer_40km_lf.txt'
                elif length == 20 and mode == 'default':
                    psd_path = psd_path/'cosmic_explorer_20km.txt'
                elif length == 20 and mode == 'post-merger':
                    psd_path = psd_path/'cosmic_explorer_20km_pm.txt'
                else:
                    raise ValueError('Invalid length', length,
                                     'and/or mode', mode)

                self.noise_psd = data.DataPSD(psd_path,
                                              psd_func=(lambda f, n: f*n**2))
                psd_path = None
                # TODO: also incorporate the analytic fit in arXiv:1809.10465
            elif noise_psd == 'Einstein Telescope':
                psd_path = Path(__file__).parent/'psds'/'ET'

                model = noise_kwargs.pop('model')

                if model == 'B':
                    model_no = '2'
                    letter = 'B'
                elif model == 'C':
                    model_no = '1'
                    letter = 'D' # probably a typo in their file names
                elif model == 'D':
                    model_no = '0'
                    letter = 'D'
                else:
                    raise ValueError('Unrecognized model:', model)

                psd_path = psd_path/('ET-000' + model_no + 'A-18_ET' + letter
                                     + 'SensitivityCurveTxtFile.txt')

                if model == 'D':
                    sub_model = noise_kwargs.pop('sub_model')
                    if sub_model == 'LF':
                        cols = (0, 1)
                    elif sub_model == 'HF':
                        cols = (0, 2)
                    elif sub_model == 'sum':
                        cols = (0, 3)
                    else:
                        raise ValueError('Unsupported sub-model:', sub_model)

                    self.noise_psd = data.DataPSD(psd_path, usecols=cols)
                    psd_path = None

            else:
                raise ValueError('Unsupported noise PSD:', noise_psd)

            if psd_path is not None:
                self.noise_psd = data.DataPSD(psd_path)

        elif detector is None:
            raise ValueError('Need to specify noise PSD')

    def snr2(self, freqs, htilde, f_0, f_1, F_p, F_c):
        h_p, h_c = htilde
        h_det = h_p*F_p + h_c*F_c

        a = np.searchsorted(freqs, f_0, side='left')
        b = np.searchsorted(freqs, f_1, side='right')

        freqs = freqs[a:b + 1]
        S_h = (np.abs(h_det)**2)[a:b + 1]

        return 4*np.trapz(S_h/self.noise_psd(freqs), freqs)
