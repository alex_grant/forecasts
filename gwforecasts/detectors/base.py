class Antenna:

    def __call__(self, ra, dec, pol, time):
        raise NotImplementedError

class Noise:

    def __call__(self, freqs):
        raise NotImplementedError
