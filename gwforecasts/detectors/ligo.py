import math

import numpy as np
import lal
from lal.antenna import AntennaResponse

from .base import Antenna, Noise

YEAR_SI = 3.154e7

class LALAntenna(Antenna):

    def __init__(self, detector, **kwargs):
        if detector == 'Custom':
            # Build a custom detector
            frdet = lal.FrDetector()
            for name in kwargs:
                setattr(frdet, name, kwargs[name])
            det = lal.Detector()
            lal.CreateDetector(det, frdet, lal.LALDETECTORTYPE_IFODIFF)
        else:
            # Hand off to LAL to see if it can interpret it
            det = detector

        # This is just the response matrix
        self.response = AntennaResponse(det, 0., math.pi/2).laldetector

    def gmstrad(self, times):
        # ...this is far too complicated for what we need here, but if it's
        # already written...
        if np.ndim(times) == 0:
            return self.gmstrad(np.array([times]))[0]

        return np.array([lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(time))
                         for time in YEAR_SI*times])

    def __call__(self, ra, dec, pol, time):
        # Like the code in DetResponse.c of LAL, this is based on Appendix B of
        # [Anderson et al., 2001] (arXiv:gr-qc/0008066)
        phi = ra - self.gmstrad(time)

        sin_phi, cos_phi = np.sin(phi), np.cos(phi)
        sin_psi, cos_psi = np.sin(pol), np.cos(pol)
        sin_dec, cos_dec = np.sin(dec), np.cos(dec)

        X = np.transpose([sin_phi*cos_psi - cos_phi*sin_psi*sin_dec,
                          -cos_phi*cos_psi - sin_phi*sin_psi*sin_dec,
                          sin_psi*cos_dec])

        Y = np.transpose([-sin_phi*sin_psi - cos_phi*cos_psi*sin_dec,
                          cos_phi*sin_psi - sin_phi*cos_psi*sin_dec,
                          cos_psi*cos_dec])

        if np.ndim(ra) == 0:
            outer, inner = 'i,j', 'ij,ij'
        else:
            outer, inner = '...i,...j', '...ij,...ij'

        e_p = np.einsum(outer, X, X) - np.einsum(outer, Y, Y)
        e_c = np.einsum(outer, X, Y) + np.einsum(outer, Y, X)

        F_p = np.einsum(inner, e_p, self.response)
        F_c = np.einsum(inner, e_c, self.response)

        return F_p, F_c

class Ajith2011PSD(Noise):

    def __call__(self, freqs):
        # Taken from [Ajith, 2011] (arXiv:1107.1267), equation (4.7), where all
        # quantities are in Hz
        x = freqs/245.4
        # I've taken the liberty of naming these parameters:
        A = 1e-48
        c_4 = 0.0152
        c_94 = 0.2935
        c_32 = 2.7951
        c_34 = -6.5080
        c_0 = 17.7622

        return A*(c_4*x**(-4) + c_94*x**(9/4) + c_32*x**(3/2) + c_34*x**(3/4)
                  + c_0)
