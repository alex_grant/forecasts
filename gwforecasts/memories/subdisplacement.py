import math
import numpy as np

from ..utils import misc, harmonics
from .base import MemoryEffect

class SubleadingDisplacementMemory(MemoryEffect):

    def coeff(self, l):
        return 1/(l*(l + 1)*math.sqrt((l + 2)*(l - 1)))

    def xi(self, l, lp, lpp, mp, mpp):
        term1 = math.sqrt((lp - 2)*(lp + 3))*self.overlap(-3, 2, l, lp, lpp,
                                                          mp, mpp)
        term2 = 3*math.sqrt((lpp + 2)*(lpp - 1))*self.overlap(-2, 1, l, lp, lpp,
                                                              mp, mpp)
        return (term1 + term2)/4

    def xi_I(self, I, l, lp, lpp, mp, mpp):
        term1 = self.xi(l, lp, lpp, mp, mpp)
        term2 = misc.sign(l + lp + lpp)*self.xi(l, lpp, lp, mpp, mp)

        if I == 'E':
            return term1 - term2

        if I == 'B':
            return 1j*(term1 + term2)

    def omega(self, S, Stilde, l, m, lp, mp):
        return misc.sign(mp)*(S[l, m]*np.conj(Stilde[lp, -mp])
                              - Stilde[l, m]*np.conj(S[lp, -mp]))

    def summand_swsh(self, I, C_lm, Cdot_lm, l, m, lp, mp, lpp):
        mpp = m - mp

        return self.xi_I(I, l, lp, lpp, mp, mpp)*self.omega(C_lm, Cdot_lm,
                                                            lp, mp, lpp, mpp)

    def Xi_swsh(self, I, C_lm, Cdot_lm, l, m):
        retval = np.zeros(shape=self.shape, dtype=self.dtype)
        max_ell = harmonics.max_ell(C_lm)

        for lp in range(2, max_ell + 1):
            for mp in range(-lp, lp + 1):
                for lpp in range(max(2, abs(l - lp), abs(m - mp)),
                                 min(max_ell, l + lp) + 1):
                    retval += self.summand_swsh(I, C_lm, Cdot_lm,
                                                l, m, lp, mp, lpp)

        return retval

    def summand_tensor(self, I, C_E_lm, C_B_lm, Cdot_E_lm, Cdot_B_lm, l, m,
                       lp, mp, lpp):
        mpp = m - mp
        term = np.array([])

        if ((I == 'E' and not (l + lp + lpp) % 2)
            or (I == 'B' and (l + lp + lpp) % 2)):
            term = (C_E_lm[lp, mp]*Cdot_E_lm[lpp, mpp]
                    + C_B_lm[lp, mp]*Cdot_B_lm[lpp, mpp])
            term *= self.xi(l, lp, lpp, mp, mpp) - self.xi(l, lpp, lp, mpp, mp)

        if ((I == 'E' and (l + lp + lpp) % 2)
            or (I == 'B' and not (l + lp + lpp) % 2)):
            term = 1j*(C_E_lm[lp, mp]*Cdot_B_lm[lpp, mpp]
                       - C_B_lm[lp, mp]*Cdot_E_lm[lpp, mpp])
            term *= self.xi(l, lp, lpp, mp, mpp) + self.xi(l, lpp, lp, mpp, mp)

        if I == 'B':
            term *= 1j

        return term

    def Xi_tensor(self, I, C_E_lm, C_B_lm, Cdot_E_lm, Cdot_B_lm, l, m):
        retval = np.zeros(shape=self.shape, dtype=self.dtype)
        max_ell = harmonics.max_ell(C_E_lm)

        for lp in range(2, max_ell + 1):
            for mp in range(-lp, lp + 1):
                for lpp in range(max(2, abs(l - lp), abs(m - mp)),
                                 min(max_ell, l + lp) + 1):
                    retval += self.summand_tensor(I, C_E_lm, C_B_lm,
                                                  Cdot_E_lm, Cdot_B_lm,
                                                  l, m, lp, mp, lpp)

        return retval
