import math
import numpy as np

from ..utils import integration
from .base import MemoryEffect

class DisplacementMemoryTest(MemoryEffect):

    def __init__(self, **kwargs):
        super(DisplacementMemoryTest, self).__init__(**kwargs)

    def integrand_22(self, Cdot_E_22, l, m):
        if (l, m) != (2, 0) and (l, m) != (4, 0):
            raise ValueError('Unsupported mode: {0}'.format((l, m)))

        retval = Cdot_E_22*np.conj(Cdot_E_22)

        # Values courtesy of [Favata, 2009]

        if l == 2:
            retval *= math.sqrt(5/(3*math.pi))/14

        if l == 4:
            retval *= math.sqrt(5/math.pi)/2520

        return retval

    def memory_swsh(self, C_lm, times):
        raise NotImplementedError('The SWSH version of the (2, 2) mode '
                                  'calculation is not implemented yet...')

    def memory_tensor(self, C_E_lm, C_B_lm, times):
        Delta_lm = {}
        dummy_lm = {}

        for l, m in {(2, 0), (4, 0)}:
            Cdot_E_22 = np.gradient(C_E_lm[2, 2], times)
            integrand = self.integrand_22(Cdot_E_22, l, m)
            du = times[1] - times[0]
            Delta_lm[l, m] = integration.indef_integrate(integrand, times,
                                                         self.ref_index,
                                                         self.int_mode)

            dummy_lm[l, m] = np.zeros_like(Delta_lm[l, m])

        return Delta_lm, dummy_lm
