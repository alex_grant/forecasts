import math
import numpy as np

from .subdisplacement import SubleadingDisplacementMemory
from ..utils import integration

class CoMMemory(SubleadingDisplacementMemory):

    # Note: this is the definition of the center-of-mass memory using a
    # definition of the "moment of the news" that does not appear in
    # [Grant & Nichols, 2021] (arXiv:2109.03832).  It is instead closer to the
    # original definition, which appears in [Nichols, 2018] (arXiv:1807.08767).

    def integrand_swsh(self, times, C_lm, Cdot_lm, l, m):
        return self.coeff(l)*self.Xi_swsh('E', C_lm, Cdot_lm, l, m)

    def memory_swsh(self, C_lm, times):
        Cdot_lm = {}

        for lm in C_lm.keys():
            Cdot_lm[lm] = np.gradient(C_lm[lm], times)

        kappa_lm = {}

        for l in range(2, self.ell_max + 1):
            for m in range(-l, l + 1):
                integrand = self.integrand_swsh(times, C_lm, Cdot_lm, l, m)
                kappa_lm[l, m] = integration.indef_integrate(integrand, times,
                                                             self.ref_index,
                                                             self.int_mode)

        return kappa_lm

    def integrand_tensor(self, times, C_E_lm, C_B_lm, Cdot_E_lm, Cdot_B_lm,
                         l, m):
        return self.coeff(l)*self.Xi_tensor('E', C_E_lm, C_B_lm,
                                            Cdot_E_lm, Cdot_B_lm, l, m)

    def memory_tensor(self, C_E_lm, C_B_lm, times):
        Cdot_E_lm, Cdot_B_lm = {}, {}

        for lm in C_E_lm.keys():
            Cdot_E_lm[lm] = np.gradient(C_E_lm[lm], times)
            Cdot_B_lm[lm] = np.gradient(C_B_lm[lm], times)

        kappa_lm, dummy_lm = {}, {}

        for l in range(2, self.ell_max + 1):
            for m in range(-l, l + 1):
                integrand = self.integrand_tensor(times, C_E_lm, C_B_lm,
                                                  Cdot_E_lm, Cdot_B_lm, l, m)
                kappa_lm[l, m] = integration.indef_integrate(integrand, times,
                                                             self.ref_index,
                                                             self.int_mode)

                dummy_lm[l, m] = np.zeros_like(kappa_lm[l, m])

        return kappa_lm, dummy_lm
