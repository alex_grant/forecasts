import math
import numpy as np

from ..utils import misc, harmonics, integration
from .base import MemoryEffect

class DisplacementMemory(MemoryEffect):

    def coeff(self, l, lp, mp, lpp, mpp):
        C = self.overlap(-2, 2, l, lp, lpp, mp, mpp)
        return C/math.sqrt((l + 2)*(l + 1)*l*(l - 1))

    def summand_swsh(self, Cdot_lm, l, m, lp, mp, lpp):
        mpp = m - mp
        term = np.array([])

        try:
            term = Cdot_lm[lp, mp]*np.conj(Cdot_lm[lpp, -mpp])
        except KeyError:
            return np.zeros(shape=self.shape, dtype=self.dtype)

        return misc.sign(mpp)*term*self.coeff(l, lp, mp, lpp, mpp)

    def integrand_swsh(self, Cdot_lm, l, m):
        retval = np.zeros(shape=self.shape, dtype=self.dtype)
        max_ell = harmonics.max_ell(Cdot_lm)

        for lp in range(2, max_ell + 1):
            for mp in range(-lp, lp + 1):
                for lpp in range(max(2, abs(l - lp), abs(m - mp)),
                                 min(max_ell, l + lp) + 1):
                    retval += self.summand_swsh(Cdot_lm, l, m, lp, mp, lpp)

        return retval

    def memory_swsh(self, C_lm, times):
        Cdot_lm = {}

        for lm in C_lm.keys():
            Cdot_lm[lm] = np.gradient(C_lm[lm], times)

        Delta_lm = {}

        for l in range(2, self.ell_max + 1):
            for m in range(-l, l + 1):
                integrand = self.integrand_swsh(Cdot_lm, l, m)
                Delta_lm[l, m] = integration.indef_integrate(integrand, times,
                                                             self.ref_index,
                                                             self.int_mode)

        return Delta_lm

    def summand_tensor(self, Cdot_E_lm, Cdot_B_lm, l, m, lp, mp, lpp):
        mpp = m - mp

        term = np.array([]);

        try:
            if (l + lp + lpp) % 2:
                term = 2*1j*Cdot_E_lm[lp, mp]*Cdot_B_lm[lpp, mpp]
            else:
                term = (Cdot_E_lm[lp, mp]*Cdot_E_lm[lpp, mpp]
                        + Cdot_B_lm[lp, mp]*Cdot_B_lm[lpp, mpp])
        except KeyError:
            return np.zeros(shape=self.shape, dtype=self.dtype)

        return self.coeff(l, lp, mp, lpp, mpp)*term/math.sqrt(2)

    def integrand_tensor(self, Cdot_E_lm, Cdot_B_lm, l, m):
        retval = np.zeros(shape=self.shape, dtype=self.dtype)
        max_ell = harmonics.max_ell(Cdot_E_lm)

        for lp in range(2, max_ell + 1):
            for mp in range(-lp, lp + 1):
                for lpp in range(max(2, abs(l - lp), abs(m - mp)),
                                 min(max_ell, l + lp) + 1):
                    retval += self.summand_tensor(Cdot_E_lm, Cdot_B_lm,
                                                  l, m, lp, mp, lpp)

        return retval

    def memory_tensor(self, C_E_lm, C_B_lm, times):
        Cdot_E_lm = {}
        Cdot_B_lm = {}

        for lm in C_E_lm.keys():
            Cdot_E_lm[lm] = np.gradient(C_E_lm[lm], times)
            Cdot_B_lm[lm] = np.gradient(C_B_lm[lm], times)

        Delta_lm = {}
        dummy_lm = {}

        for l in range(2, self.ell_max + 1):
            for m in range(-l, l + 1):
                integrand = self.integrand_tensor(Cdot_E_lm, Cdot_B_lm, l, m)
                Delta_lm[l, m] = integration.indef_integrate(integrand, times,
                                                             self.ref_index,
                                                             self.int_mode)
                dummy_lm[l, m] = np.zeros_like(Delta_lm[l, m])

        return Delta_lm, dummy_lm
