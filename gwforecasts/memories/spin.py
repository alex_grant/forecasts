import math
import numpy as np

from .subdisplacement import SubleadingDisplacementMemory

class SpinMemory(SubleadingDisplacementMemory):

    def memory_swsh(self, C_lm, times):
        Cdot_lm = {}

        for lm in C_lm.keys():
            Cdot_lm[lm] = np.gradient(C_lm[lm], times)

        Sigma_lm = {}

        for l in range(2, self.ell_max + 1):
            for m in range(-l, l + 1):
                Sigma_lm[l, m] = self.coeff(l)*self.Xi_swsh('B', C_lm, Cdot_lm,
                                                            l, m)*(-1j)

        return Sigma_lm

    def memory_tensor(self, C_E_lm, C_B_lm, times):
        Cdot_E_lm, Cdot_B_lm = {}, {}

        for lm in C_E_lm.keys():
            Cdot_E_lm[lm] = np.gradient(C_E_lm[lm], times)
            Cdot_B_lm[lm] = np.gradient(C_B_lm[lm], times)

        Sigma_lm, dummy_lm = {}, {}

        for l in range(2, self.ell_max + 1):
            for m in range(-l, l + 1):
                Sigma_lm[l, m] = self.coeff(l)*self.Xi_tensor('B',
                                                              C_E_lm, C_B_lm,
                                                              Cdot_E_lm,
                                                              Cdot_B_lm,
                                                              l, m)*math.sqrt(2)
                dummy_lm[l, m] = np.zeros_like(Sigma_lm[l, m])

        return dummy_lm, Sigma_lm
