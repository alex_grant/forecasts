import math
import numpy as np

from ..utils import harmonics

class MemoryEffect:

    def __init__(self, ell_max=2, ref_index=0, mode='SWSH', int_mode='ctrapz',
                 overlap_cache=None, _22=False):

        self.shape = None
        self.dtype = None
        self.ell_max = ell_max
        self.ref_index = ref_index
        self.int_mode = int_mode

        if mode != 'SWSH' and mode != 'Tensor':
            raise ValueError('Unsupported computation mode:', mode)

        self.mode = mode
        self.overlap_cache = overlap_cache
        self._22 = _22

    def overlap(self, sp, spp, l, lp, lpp, mp, mpp):
        if self.overlap_cache is not None:
            return self.overlap_cache[sp, spp][l, lp, lpp][mp, mpp]

        return harmonics.swsh_overlap(sp, spp, l, lp, lpp, mp, mpp)

    def process(self, C_lm, times):
        # TODO: implement checks to ensure that there are sufficient $\ell$
        # modes in the input in order to compute up to a certain $\ell$ mode in
        # the output...?

        self.shape = next(iter(C_lm.values())).shape
        self.dtype = next(iter(C_lm.values())).dtype

        if self._22:
            C_lm = {lm: C_lm[lm] if lm in ((2, -2), (2, 2))
                    else np.zeros(self.shape, dtype=self.dtype)
                    for lm in C_lm}

        if self.mode == 'SWSH':
            return self.memory_swsh(C_lm, times)

        C_E_lm, C_B_lm = harmonics.tensor(C_lm)

        mem_E, mem_B = self.memory_tensor(C_E_lm, C_B_lm, times)

        retval = {}

        # Note: mem_E.keys() and mem_B.keys() need to be the same!
        for lm in mem_E.keys():
            retval[lm] = (mem_E[lm] - 1j*mem_B[lm])/math.sqrt(2)

        return retval

    def memory_swsh(self, C_lm, times):
        raise NotImplementedError

    def memory_tensor(self, C_E_lm, C_B_lm, times):
        raise NotImplementedError
