import math
import numpy as np
from bisect import bisect_left, bisect_right

from ..utils import integration
from .base import Moment

class RollingAverage(Moment):

    def __init__(self, *, transition_width=1, scale=1, alpha=1., **kwargs):
        super().__init__(**kwargs)
        self.transition_width = transition_width
        self.scale = scale
        self.alpha = alpha

    def approximation(self, moment, times):
        retval = np.zeros_like(moment)
        spans = self.transition(times, moment)
        spans *= self.scaling(times, moment)
        spans = np.floor(spans).astype(int)

        int_mode = self.int_mode
        if int_mode[0] != 'c':
            int_mode = 'c' + int_mode

        ints = integration.indef_integrate(moment, times, 0, int_mode)

        for i, u in enumerate(times):
            if spans[i] == 0 or i == self.ref_index:
                retval[i] = moment[i]
            else:
                left_span = math.floor((1 - self.alpha)*spans[i])
                right_span = spans[i] - left_span

                start_idx = max(0, i - left_span)
                end_idx = min(len(times) - 1, i + right_span)

                if start_idx == end_idx:
                    retval[i] = moment[i]
                else:
                    retval[i] = ints[end_idx] - ints[start_idx]
                    width = times[end_idx] - times[start_idx]
                    retval[i] /= width

        return retval

    def transition(self, times, moment):
        return np.ones_like(times)

    def scaling(self, times, moment):
        return np.full_like(times, self.scale)

class ExponentialTransition(RollingAverage):

    def transition(self, times, moment):
        ones = np.ones_like(times)
        indices = np.array(range(len(times))) - self.ref_index
        return ones - np.exp(-(indices/self.transition_width)**2/2)

class BumpTransition(RollingAverage):

    def transition(self, times, moment):
        retval = np.ones_like(times)
        for i in range(len(retval)):
            idx = i - self.ref_index
            if -self.transition_width < idx < self.transition_width:
                retval[i] -= np.exp(idx**2/(idx**2 - self.transition_width**2))

        return retval

class PNScaling(RollingAverage):

    # This scaling comes from [Cutler & Flanagan, 1994]

    def __init__(self, q=1., **kwargs):
        super().__init__(**kwargs)

        if q > 1:
            q = 1/q

        # Note: we're using units where $M = 1$!
        # Todo: figure out if this $M$ is total mass or primary mass...

        self.chirp_53 = q/(1 + q)
        # Period at the ISCO
        self.min_period = pow(6, 3/2)*np.pi

    def period(self, u):
        # The period of the gravitational wave as a function of time
        if u < 0:
            return max(8*np.pi*pow(-u*self.chirp_53/5, 3/8), self.min_period)

        return self.min_period

    def scaling(self, times, moment):
        retval = np.zeroes_like(times)

        for i, u in enumerate(times):
            period = self.period(u)
            start_idx = bisect_left(times, u - (1 - self.alpha)*period)
            end_idx = bisect_right(times, u + self.alpha*period)
            retval[i] = end_idx - start_idx

class InstantaneousScaling(RollingAverage):

    def scaling(self, times, moment):
        diff = np.gradient(moment, times)

        retval = np.zeros_like(times)

        for i, (u, A, dA) in enumerate(zip(times, moment, diff)):
            if np.abs(A) == 0. or np.abs(dA) == 0.:
                retval[i] = 0
            else:
                period = 2*np.pi*np.abs(A/dA)
                start_idx = bisect_left(times, u - (1 - self.alpha)*period)
                end_idx = bisect_right(times, u + self.alpha*period)
                retval[i] = end_idx - start_idx

        return retval
