import numpy as np

from .base import Moment
from ..memories.displacement import DisplacementMemory

class FullWaveform(Moment):

    def process(self, C_lm, times):
        disp = DisplacementMemory(ell_max=self.ell_max)

        mem_lm = disp.process(C_lm, times)

        new_C_lm = {}

        for lm in mem_lm.keys():
            new_C_lm[lm] = C_lm[lm] + mem_lm[lm]

        return super().process(new_C_lm, times)
