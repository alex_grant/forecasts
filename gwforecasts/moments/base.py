import math
import numpy as np

from ..utils import integration, harmonics
from ..memories.base import MemoryEffect

class Moment(MemoryEffect):

    def __init__(self, n=0, **kwargs):
        super().__init__(**kwargs)
        self.n = n

    def memory_swsh(self, C_lm, times):
        retval = {}

        for lm in C_lm.keys():
            moment = self.moment_generator(C_lm[lm], times)
            approx = self.approximation(moment, times)
            retval[lm] = self.moment_inversion(approx, times)
            retval[lm] += C_lm[lm][self.ref_index]

        return retval

    def memory_tensor(self, C_E_lm, C_B_lm, times):
        retval = self.memory_swsh(harmonics.swsh(C_E_lm, C_B_lm), times)
        return harmonics.tensor(retval)

    def moment_inversion(self, moment, times):
        factor = np.power(times - times[self.ref_index], -self.n)
        factor[self.ref_index] = 0.

        return integration.indef_integrate(factor*np.gradient(moment, times),
                                           times, self.ref_index, self.int_mode)

    def approximation(self, moment, times):
        return moment

    def moment_generator(self, shear, times):
        factor = np.power(times - times[self.ref_index], self.n)
        factor[self.ref_index] = 0.

        return integration.indef_integrate(factor*np.gradient(shear, times),
                                           times, self.ref_index, self.int_mode)
