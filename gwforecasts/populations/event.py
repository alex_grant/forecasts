from inspect import signature, Parameter
import math

import numpy as np
import astropy.units as units

from .multi import MCMCSampler
from .rate import RateSampler
from ..utils.spin import cartesian

class SubSampler(MCMCSampler):

    def __init__(self, *, combo_samplers, internal_params,
                 walkers, **kwargs):

        self.combo_samplers = combo_samplers
        self.internal_params = internal_params
        self.__dict__.update(kwargs)

        super().__init__(walkers=walkers, dim=len(internal_params))

    def pdf(self, **kwargs):
        retval = 1.
        for sampler in self.combo_samplers:
            retval *= sampler.pdf(**kwargs)

        return retval

    def pdf_truncate(self, **kwargs):
        if ('m_1' in self.internal_params
            and (kwargs['m_1'] < self.m_min or kwargs['m_1'] > self.m_max)):
            return 0.

        if ('q' in self.internal_params
            and (kwargs['q']*kwargs['m_1'] < self.m_min or kwargs['q'] > 1)):
            return 0.

        if ('z' in self.internal_params
            and (kwargs['z'] < 0 or kwargs['z'] > self.z_max)):
            return 0.

        if ('chi_1' in self.internal_params
            and (kwargs['chi_1'] < 0 or kwargs['chi_1'] > 1
                 or kwargs['chi_2'] < 0 or kwargs['chi_2'] > 1)):
            return 0.

        if ('cos_1' in self.internal_params
            and (kwargs['cos_1'] < -1 or kwargs['cos_1'] > 1
                 or kwargs['cos_2'] < -1 or kwargs['cos_2'] > 1)):
            return 0.

        return self.pdf(**kwargs)

    def log_pdf(self, x):
        return np.log(self.pdf_truncate(**self.x_to_internal_params(x)))

    def x_to_internal_params(self, x):
        if len(x) != len(self.internal_params):
            raise ValueError('Samples must have the same length as the '
                             'internal parameters')
        return {param: x[i] for (i, param) in enumerate(self.internal_params)}

    def internal_params_to_x(self, **params):
        return [params[param] for param in self.internal_params]

    def initial_state(self):
        retval = {}
        if 'm_1' in self.internal_params:
            retval['m_1'] = np.random.uniform(self.m_min, self.m_max)
            if 'q' in self.internal_params:
                retval['q'] = np.random.uniform(self.m_min/retval['m_1'], 1)

        if 'z' in self.internal_params:
            retval['z'] = np.random.uniform(0., self.z_max)

        if 'chi_1' in self.internal_params:
            retval['chi_1'] = np.random.uniform(0, 1)
            retval['chi_2'] = np.random.uniform(0, 1)

        if 'cos_1' in self.internal_params:
            retval['cos_1'] = np.random.uniform(-1, 1)
            retval['cos_2'] = np.random.uniform(-1, 1)

        return self.internal_params_to_x(**retval)

    def draw_internals(self, n):
        return [self.x_to_internal_params(x) for x in self.draw(n)]

class EventSampler:

    def __init__(self, *, sampler_classes, walkers, max_events=np.inf,
                 **kwargs):
        self.direct_samplers = []
        subsampler_args = []
        for cls in sampler_classes:
            pdf_param_set = set()
            pdf_sig = signature(cls.pdf)
            combo_sampler = cls(**kwargs)

            if issubclass(cls, RateSampler):
                # Extract these useful things...
                self.rate = combo_sampler.rate
                self.cosmology = combo_sampler.cosmology

            single = combo_sampler.direct_draw(1)
            if single != {}:
                self.direct_samplers.append(combo_sampler)

            for param in pdf_sig.parameters:
                if pdf_sig.parameters[param].kind is Parameter.KEYWORD_ONLY:
                    pdf_param_set.add(param)

            if pdf_param_set == set():
                continue

            for subsampler in subsampler_args:
                if subsampler['internal_params'] & pdf_param_set:
                    # One of the new params is part of the params of one of the
                    # samplers, so the class should be added to that sampler
                    subsampler['combo_samplers'].append(combo_sampler)
                    subsampler['internal_params'].update(pdf_param_set)
                    break
            else:
                subsampler_args.append({'internal_params': pdf_param_set,
                                        'combo_samplers': [combo_sampler]})

        self.subsamplers = []

        for args in subsampler_args:
            sampler = SubSampler(walkers=walkers,
                                 internal_params=tuple(args['internal_params']),
                                 combo_samplers=args['combo_samplers'],
                                 **kwargs)
            sampler.burnin()
            self.subsamplers.append(sampler)

        self.max_events = max_events

    def draw(self, n):
        retval = [{} for i in range(n)]
        for sampler in self.subsamplers:
            samples = sampler.draw_internals(n)
            # Fix the arrays if they're different lengths
            if len(samples) < len(retval):
                del retval[len(samples):]
            if len(samples) > len(retval):
                del samples[len(retval):]

            for i, sample in enumerate(samples):
                retval[i].update(sample)

        for sampler in self.direct_samplers:
            samples = sampler.direct_draw(len(retval))
            for key in samples:
                for i, sample in enumerate(samples[key]):
                    retval[i][key] = sample

        return retval

    def convert_to_external(self, **kwargs):
        retval = {key: kwargs[key]
                  for key in ('q', 'iota', 'phi', 'delta', 'alpha', 'psi', 'z')}

        retval.update({'M': (1 + kwargs['z'])*(1 + kwargs['q'])*kwargs['m_1'],
                       'dist': self.z_to_dist(kwargs['z']),
                       'T': self.increment(kwargs['dt'])})

        if 'chi_1' in kwargs:
            retval.update({'chi_1': cartesian(kwargs['chi_1'], kwargs['cos_1'],
                                              kwargs['phi_1']),
                           'chi_2': cartesian(kwargs['chi_2'], kwargs['cos_2'],
                                              kwargs['phi_2'])})

        return retval

    def increment(self, dt):
        self.T += dt
        return self.T

    def z_to_dist(self, z):
        return self.cosmology.luminosity_distance(z).to(units.Mpc).value

    def draw_events(self, T_max=None, N_max=None):
        self.T = 0.

        events = []

        if T_max is not None:
            def remainder():
                return int(math.ceil(self.rate*(T_max - self.T)))
        elif N_max is not None:
            def remainder():
                return N_max - len(events)
        else:
            raise ValueError('One of T_max and N_max must be specified')

        while True:
            n = min(remainder(), self.max_events)
            if n <= 0:
                break
            events.extend([self.convert_to_external(**params)
                           for params in self.draw(n)])

        return events
