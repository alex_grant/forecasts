import numpy as np
from scipy.stats import uniform

from .combo import ComboSampler

class Uniform(ComboSampler):

    def direct_draw(self, n):
        cos_iota = uniform.rvs(loc=-1, scale=2, size=n)
        phi = uniform.rvs(scale=2*np.pi, size=n)
        sin_delta = uniform.rvs(loc=-1, scale=2, size=n)
        alpha = uniform.rvs(scale=2*np.pi, size=n)
        psi = uniform.rvs(scale=np.pi, size=n)

        return { 'iota' : np.arccos(cos_iota),
                 'phi' : phi,
                 'delta' : np.arcsin(sin_delta),
                 'alpha' : alpha,
                 'psi' : psi }
