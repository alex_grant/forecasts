import math

import numpy as np
from scipy.stats import beta
from scipy.stats import uniform
from scipy.stats import truncnorm

from .combo import ComboSampler
from . import distributions

class SpinSampler(ComboSampler):

    def direct_draw(self, n):
        return {'phi_1': uniform.rvs(scale=2*np.pi, size=n),
                'phi_2': uniform.rvs(scale=2*np.pi, size=n)}

class Default(SpinSampler):

    def __init__(self, *, mu_chi, sigma_chi2, **kwargs):
        super().__init__(**kwargs)
        self.alpha_chi = (1 - mu_chi)/sigma_chi2 - 1/mu_chi
        self.beta_chi = self.alpha_chi*(1/mu_chi - 1)

    def direct_draw(self, n):
        params = uniform.rvs(size=n)

        trunc_gauss = truncnorm(-2/self.sigma_t, 0, loc=1, scale=self.sigma_t)
        unif = uniform(scale=2, loc=-1)

        dist_1 = np.stack((trunc_gauss.rvs(size=n), trunc_gauss.rvs(size=n)),
                          axis=-1)
        dist_2 = np.stack((unif.rvs(size=n), unif.rvs(size=n)), axis=-1)

        selected = np.where((params < self.zeta).reshape(-1, 1), dist_1, dist_2)

        return {**super().direct_draw(n),
                'cos_1': selected[:, 0],
                'cos_2': selected[:, 1],
                'chi_1': beta.rvs(self.alpha_chi, self.beta_chi, size=n),
                'chi_2': beta.rvs(self.alpha_chi, self.beta_chi, size=n)}

class Uniform(SpinSampler):

    def __init__(self, *, magnitude=True, **kwargs):
        super().__init__(magnitude=magnitude, **kwargs)

    def direct_draw(self, n):
        if self.magnitude:
            chi_1 = uniform.rvs(size=n)
            chi_2 = uniform.rvs(size=n)
        else: # uniform in volume
            chi_1 = np.power(uniform.rvs(size=n), 1/3)
            chi_2 = np.power(uniform.rvs(size=n), 1/3)

        return {**super().direct_draw(n),
                'chi_1': chi_1,
                'chi_2': chi_2,
                'cos_1': uniform.rvs(loc=-1, scale=2, size=n),
                'cos_2': uniform.rvs(loc=-1, scale=2, size=n)}
