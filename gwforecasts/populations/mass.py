from scipy.stats import uniform

from .combo import ComboSampler

class Uniform(ComboSampler):

    def direct_draw(self, n):
        m_1 = uniform.rvs(loc=self.m_min, scale=(self.m_max - self.m_min),
                          size=n)
        return {'m_1': m_1,
                'q': ((m_1 - self.m_min)*uniform.rvs(size=n)
                      + self.m_min)/m_1}
