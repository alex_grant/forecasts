import math

from .combo import ComboSampler
from ..utils import misc
from . import distributions

class Truncated(ComboSampler):

    def pdf(self, *, q, m_1, **kwargs):
        return distributions.power_law(q, self.beta_q, self.m_min/m_1, 1)

class Smooth(ComboSampler):

    def pdf(self, *, q, m_1, **kwargs):
        return (distributions.power_law(q, self.beta_q, self.m_min/m_1, 1)
                * misc.smooth(q*m_1, self.m_min, self.delta_m))

