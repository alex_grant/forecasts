import math
import numpy as np
from scipy.stats import expon
from scipy.stats import uniform
import astropy.units as units

from .combo import ComboSampler

class RateSampler(ComboSampler):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.zs = np.linspace(0, self.z_max, num=self.z_samples)
        self.dV_dzs = np.array([(self.cosmology.differential_comoving_volume(z)
                                 *units.sr).to(units.Gpc**3).value
                                for z in self.zs])*4*np.pi

        integrand = self.R_0*self.z_pdf(self.zs, self.dV_dzs)
        self.rate = np.trapz(integrand, self.zs)

    def pdf(self, *, z, **kwargs):
        dV_dz = np.interp(z, self.zs, self.dV_dzs)
        return self.z_pdf(z, dV_dz)

    def direct_draw(self, n):
        return {'dt': expon.rvs(size=n, scale=1/self.rate)}

    def z_pdf(self, z, dV_dz):
        raise NotImplementedError

class PowerLaw(RateSampler):

    def z_pdf(self, z, dV_dz):
        return np.power(1 + z, self.kappa - 1)*dV_dz

class UniformTimeless(RateSampler):

    def __init__(self, **kwargs):
        ComboSampler.__init__(self, **kwargs)
        self.rate = np.infty

    def pdf(self, **kwargs):
        pass

    def direct_draw(self, n):
        return {'dt': np.zeros(shape=n),
                'z': uniform.rvs(scale=self.z_max, size=n)}
