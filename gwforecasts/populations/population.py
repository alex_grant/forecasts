import json
import numpy as np
import h5py

from . import multi

class PopulationSampler(multi.BoundedKDESampler):

    def __init__(self, *, filename, var_map, release,
                 **kwargs):

        if release == 'GWTC-2':
            with open(filename, 'r') as file:
                jdict = json.load(file)
                self.load(jdict['posterior']['content'], var_map, kwargs)

        elif release == 'GWTC-1':
            with h5py.File(filename, 'r') as file:
                self.load(file['hyperparams'], var_map, kwargs)

        else:
            raise ValueError('Unrecognized release', release)

    def load(self, samples, var_map, kwargs):
        N = len(samples[next(iter(var_map))])

        data = np.zeros(shape=(N, len(var_map)))

        bounds = []

        self.var_map = {}
        for j, key in enumerate(var_map.keys()):
            self.var_map[var_map[key][0]] = j
            bounds.append(var_map[key][1:])
            for i, value in enumerate(samples[key]):
                data[i][j] = value

        super().__init__(data=data, bounds=bounds, **kwargs)

    def draw_populations(self, n):
        samples = self.draw(n)
        return [{key: sample[self.var_map[key]] for key in self.var_map}
                for sample in samples]

class ModelSampler:

    def __init__(self, filename, *, release, model, z_max,
                 include_spins=True, evolve_redshift=False,
                 processes):
        self.z_max = z_max

        self.var_map = {}

        self.samplers = {'rate': 'PowerLaw'}

        if release == 'GWTC-2':

            if include_spins:
                self.samplers['spin'] = 'Default'

                self.var_map.update({'mu_chi': ('mu_chi', 0, 1),
                                     'sigma_chi': ('sigma_chi2', 0, np.inf),
                                     'xi_spin': ('zeta', 0, 1),
                                     'sigma_spin': ('sigma_t', 0, np.inf)})

            if evolve_redshift:
                self.var_map.update({'lamb': ('kappa', -np.inf, np.inf),
                                     'rate': ('R_0', 0, np.inf)})
            else:
                self.var_map.update({'rate': ('R_0', 0, np.inf)})

            if model == 'B':
                self.samplers['primary'] = 'Truncated'
                self.samplers['mass_ratio'] = 'Truncated'

                self.var_map.update({'alpha': ('alpha', -np.inf, np.inf),
                                     'beta': ('beta_q', -np.inf, np.inf),
                                     'mmin': ('m_min', 0, np.inf),
                                     'mmax': ('m_max', 0, np.inf)})
            elif model == 'C':
                self.samplers['primary'] = 'PowerLawPeak'
                self.samplers['mass_ratio'] = 'Smooth'

                self.var_map.update({'alpha': ('alpha', -np.inf, np.inf),
                                     'beta': ('beta_q', -np.inf, np.inf),
                                     'delta_m': ('delta_m', 0, np.inf),
                                     'mmin': ('m_min', 0, np.inf),
                                     'mmax': ('m_power_max', 0, np.inf),
                                     'lam': ('lambda_peak', 0, 1),
                                     'mpp': ('mu_m', 0, np.inf),
                                     'sigpp': ('sigma_m', 0, np.inf)})
            elif model == 'D':
                self.samplers['primary'] = 'BrokenPowerLaw'
                self.samplers['mass_ratio'] = 'Smooth'

                self.var_map.update({'alpha_1': ('alpha_1', -np.inf, np.inf),
                                     'alpha_2': ('alpha_2', -np.inf, np.inf),
                                     'beta': ('beta_q', -np.inf, np.inf),
                                     'delta_m': ('delta_m', 0, np.inf),
                                     'mmin': ('m_min', 0, np.inf),
                                     'mmax': ('m_max', 0, np.inf),
                                     'break_fraction': ('b', 0, 1)})
            elif model == 'E':
                self.samplers['primary'] = 'MultiPeak'
                self.samplers['mass_ratio'] = 'Smooth'

                self.var_map.update({'alpha': ('alpha', -np.inf, np.inf),
                                     'beta': ('beta_q', -np.inf, np.inf),
                                     'delta_m': ('delta_m', 0, np.inf),
                                     'mmin': ('m_min', 0, np.inf),
                                     'mmax': ('m_power_max', 0, np.inf),
                                     'lam': ('lambda_peak', 0, 1),
                                     'lam_1': ('lambda_1', 0, 1),
                                     'mpp_1': ('mu_m_1', 0, np.inf),
                                     'sigpp_1': ('sigma_m_1', 0, np.inf),
                                     'mpp_2': ('mu_m_2', 0, np.inf),
                                     'sigpp_2': ('sigma_m_2', np.inf)})
            else:
                raise ValueError('Invalid model for GWTC-2:', model)

        elif release == 'GWTC-1':
            if include_spins:
                raise NotImplementedError('TODO')

            if evolve_redshift:
                raise ValueError('Evolving redshifts are not part of GWTC-1')

            self.var_map.update({'log10_rate': ('log10_R_0', -np.inf, np.inf)})

            if model in ('A', 'B'):
                self.samplers['primary'] = 'Truncated'
                self.samplers['mass_ratio'] = 'Truncated'

                self.var_map.update({'alpha_m': ('alpha', -np.inf, np.inf),
                                     'm_max': ('m_max', 0, np.inf)})

                if model == 'B':
                    self.var_map.update({'beta_q': ('beta_q', -np.inf, np.inf),
                                         'm_min': ('m_min', 0, np.inf)})
                else:
                    self.m_min = 5

            else:
                raise ValueError('Invalid model for GWTC-1:', model)

        self.sampler = PopulationSampler(filename=filename,
                                         var_map=self.var_map,
                                         release=release,
                                         processes=processes)

    def draw(self, n):
        samples = self.sampler.draw_populations(n)
        retval = []

        for sample in samples:
            model = {'samplers': self.samplers,
                     'params': sample}

            if 'kappa' not in sample:
                model['params']['kappa'] = 0.

            if 'beta_q' not in sample:
                model['params']['beta_q'] = 0.

            if 'm_min' not in sample:
                model['params']['m_min'] = self.m_min

            if 'log10_R_0' in sample:
                model['params']['R_0'] = pow(10,
                                             model['params'].pop('log10_R_0'))

            if self.samplers['primary'] in ('PowerLawPeak', 'MultiPeak'):
                model['params']['m_max'] = 100.

            model['params']['z_max'] = self.z_max

            retval.append(model)

        return retval
