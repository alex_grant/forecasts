import math

from .combo import ComboSampler
from ..utils import misc
from . import distributions

class Truncated(ComboSampler):

    def pdf(self, *, m_1, **kwargs):
        return math.pow(m_1, -self.alpha)

class PowerLawPeak(ComboSampler):

    def pdf(self, *, m_1, **kwargs):
        power_law = distributions.power_law(m_1, -self.alpha,
                                            self.m_min, self.m_power_max)
        gaussian = distributions.trunc_gaussian(m_1,
                                                self.mu_m, self.sigma_m,
                                                self.m_min, self.m_max)

        return (((1 - self.lambda_peak)*power_law + self.lambda_peak*gaussian)
                * misc.smooth(m_1, self.m_min, self.delta_m))

class BrokenPowerLaw(ComboSampler):

    def __init__(self, *, b, **kwargs):
        super().__init__(kwargs)
        self.m_break = self.m_min + b*(self.m_max - self.m_min)

    def pdf(self, *, m_1, **kwargs):
        retval = 1.
        if m_1 <= self.m_break:
            return (distributions.power_law(m_1, -self.alpha_1,
                                            self.m_min, self.m_max)
                    * misc.smooth(m_1, self.m_min, self.delta_m))

        # Note: this normalizes such that the two formulas match for
        # $m_1 = m_{\rm break}$
        scale = distributions.power_law(self.m_break, -self.alpha_1,
                                        self.m_min, self.m_max)
        scale /= distributions.power_law(self.m_break, -self.alpha_2,
                                         self.m_min, self.m_max)

        return scale*distributions.power_law(m_1, -self.alpha_2,
                                             self.m_min, self.m_max)

class MultiPeak(ComboSampler):

    def pdf(self, *, m_1, **kwargs):
        power_law = distributions.power_law(m_1, -self.alpha,
                                            self.m_min, self.m_power_max)

        peak_1 = distributions.trunc_gaussian(m_1, self.mu_m_1, self.sigma_m_1,
                                              self.m_min, self.m_max)

        peak_2 = distributions.trunc_gaussian(m_1, self.mu_m_2, self.sigma_m_2,
                                              self.m_min, self.m_max)

        return (((1 - self.lambda_peak)*power_law
                 + self.lambda_peak*(self.lambda_1*peak_1
                                     + (1 - self.lambda_1)*peak_2))
                * misc.smooth(m_1, self.m_min, self.delta_m))
