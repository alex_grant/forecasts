import atexit
import logging
import math
import multiprocessing

import numpy as np
import kombine
import emcee

class MCMCSampler:

    def __init__(self, *, walkers, dim):
        self.sampler = emcee.EnsembleSampler(walkers, dim, self)
        self.walkers = walkers
        self.dim = dim

    def advance(self, N):
        self.state = self.sampler.run_mcmc(self.state, N)

    def burnin(self, **kwargs):
        # Note: we burn in at the start to get a good initial guess of the
        # autocorrelation time and where to start the walkers
        self.state = [self.initial_state() for walker in range(self.walkers)]

        old_acl_est = 10

        while True:
            mcmc_length = 50*old_acl_est

            self.advance(50*old_acl_est)

            # logging.disable(level=logging.WARNING)
            self.acl_est = max(self.sampler.get_autocorr_time(quiet=True))
            # logging.disable(level=logging.NOTSET)
            self.acl_est = int(math.ceil(self.acl_est))

            self.sampler.reset()

            if (self.acl_est - old_acl_est)/self.acl_est < 0.05:
                self.acl_est = max(self.acl_est, old_acl_est)
                return

            old_acl_est = self.acl_est

    def draw(self, n=1):
        mcmc_length = max(n*self.acl_est // self.walkers, 1)
        self.advance(mcmc_length)

        samples = (self.sampler.get_chain()[-1] if mcmc_length < self.acl_est
                   else self.sampler.get_chain(flat=True, thin=self.acl_est))

        self.sampler.reset()

        return samples

    def __call__(self, x):
        return self.log_pdf(x)

    def __getstate__(self):
        self_dict = self.__dict__.copy()
        del self_dict['sampler']
        return self_dict

    def initial_state(self):
        raise NotImplementedError

    def log_pdf(self, x):
        raise NotImplementedError

class KDESampler:

    def __init__(self, *, data, processes):
        pool = multiprocessing.Pool(processes)
        atexit.register(pool.close)
        self.sampler = kombine.optimized_kde(data, pool)

    def draw(self, n):
        return self.sampler.draw(n)

class BoundedKDESampler(KDESampler):

    def __init__(self, *, bounds, **kwargs):
        super().__init__(**kwargs)
        self.bounds = bounds

    def within_bounds(self, sample):
        for i, entry in enumerate(sample):
            if not (self.bounds[i][0] <= entry <= self.bounds[i][1]):
                return False

        return True

    def draw(self, n):
        retval = []

        k = n

        while k > 0:
            trial = super().draw(k)
            for sample in trial:
                if self.within_bounds(sample):
                    retval.append(sample)
                    k -= 1

        return retval
