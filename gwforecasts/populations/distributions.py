import math
import numpy as np
from scipy.special import erf
# A few normalized distributions

def power_law(x, alpha, min, max):
    if min <= x and x <= max:
        norm = (pow(max, alpha + 1) - pow(min, alpha + 1))/(alpha + 1)
        return pow(x, alpha)/norm

    return 0.

def power_law_cdf(x, alpha, min, max):
    if min < x:
        if x < max:
            norm = pow(max, alpha + 1) - pow(min, alpha + 1)
            return (pow(x, alpha + 1) - pow(min, alpha + 1))/norm

        return 1.

    return 0.

def _gaussian_scale(x, mu, sigma):
    return (x - mu)/(sigma*math.sqrt(2))

def trunc_gaussian(x, mu, sigma, min, max):
    if min <= x and x <= max:
        norm = sigma*math.sqrt(math.pi/2)*(erf(_gaussian_scale(max, mu, sigma))
                                           - erf(_gaussian_scale(min, mu,
                                                                 sigma)))
        return np.exp(-_gaussian_scale(x, mu, sigma)**2)/norm

    return 0.

def trunc_gaussian_cdf(x, mu, sigma, min, max):
    if min < x:
        if x < max:
            norm = (erf(_gaussian_scale(max, mu, sigma))
                    - erf(_gaussian_scale(min, mu, sigma)))

            return (erf(_gaussian_scale(x, mu, sigma))
                    - erf(_gaussian_scale(min, mu, sigma)))/norm

        return 1.

    return 0.
