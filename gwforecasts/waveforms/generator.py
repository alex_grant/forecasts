import numpy as np
import gwtools as gwt

from ..utils import misc

class WaveformGenerator:

    def __init__(self, units='dimensionless', _22=False, roll_on=None,
                 fake_neg_modes=False, ell_max=2,
                 full_waveform=False, remove_spins=False):
        self._22 = _22
        self.units = units
        self.roll_on = roll_on
        self.fake_neg_modes = fake_neg_modes
        self.ell_max = ell_max
        self.full_waveform = full_waveform
        self.remove_spins = remove_spins

    def waveform(self, *, f_samp, M=None, dist=None, f_low, f_ref=None, q,
                 **wf_kwargs):
        if q < 1:
            q = 1/q

        if self.units == 'dimensionless':
            if (M is not None) or (dist is not None):
                raise ValueError('If units are dimensionless, then M and dist '
                                 'must not be specified.')

            self.u_scale, self.h_scale = 1., 1.
        else:
            if (M is None) or (dist is None):
                raise ValueError('If units are not dimensionless, then M and '
                                 'dist must be specified.')

            if self.units == 'mks':
                self.u_scale = M*gwt.Msuninsec
                self.h_scale = M*gwt.Msuninsec*gwt.c/(dist*1e6*gwt.PC_SI)
            else:
                raise ValueError('Unrecognized unit system:', self.units)

        if self.remove_spins:
            if 'chi_1' in wf_kwargs:
                wf_kwargs['chi_1'] = [0, 0, 0]

            if 'chi_2' in wf_kwargs:
                wf_kwargs['chi_2'] = [0, 0, 0]

        if f_ref is None:
            f_ref = f_low

        us, h_lms, scaled = self._waveform(M, dist, f_low, f_ref, 1/f_samp, q,
                                           **wf_kwargs)

        if scaled:
            us /= self.u_scale

        smoothing = None

        if self.roll_on is not None:
            smoothing = np.array([misc.smooth(u, x_0 = us[0],
                                              delta=self.roll_on/self.u_scale)
                                  for u in us])

        for l in range(2, self.ell_max + 1):
            for m in range(-l, l + 1):
                if (l, m) in h_lms.keys():
                    if scaled:
                        h_lms[l, m] /= self.h_scale

                    if smoothing is not None:
                        # Apply a smooth roll-on
                        h_lms[l, m] *= smoothing

                    if self._22:
                        # Remove all but the $2(\pm 2)$ modes of the waveform
                        # (this is useful for sanity checks)
                        if l != 2 or (m != 2 or m != -2):
                            h_lms[l, m] = np_zeros_like(h_lms[2, 2])

                else:
                    # Set the modes for $l <= l_max$ to zero if they are not
                    # present.
                    # TODO: this probably will cause trouble at some point, so
                    # fix before that happens...
                    h_lms[l, m] = np.zeros_like(h_lms[2, 2])

                    if self.fake_neg_modes:
                        # Turns out that gwsurrogate doesn't always implement
                        # this, so we'll do it here--note that you shouldn't set
                        # this option to True for precessing systems!
                        if m < 0 and (l, -m) in h_lms.keys():
                            h_lms[l, m] = misc.sign(l)*np.conj(h_lms[l, -m])

        return us, h_lms

    def _waveform(self, M, dist, f_low, f_ref, du, q, **kwargs):
        raise NotImplementedError

    def rescale_h(self, h):
        retval = {}
        for lm in h.keys():
            retval[lm] = h[lm]*self.h_scale

        return retval

    def rescale_u(self, u):
        return u*self.u_scale
