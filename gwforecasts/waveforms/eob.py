import numpy as np
import lalsimulation as lalsim
from gwtools import Msuninkg, PC_SI

from .generator import WaveformGenerator

class EOBGenerator(WaveformGenerator):

    def __init__(self, eob_model, **kwargs):
        super().__init__(**kwargs)
        self.approx = lalsim.GetApproximantFromString(eob_model)

    def _waveform(self, M, dist, f_low, f_ref, du, q,
                  *, chi_1, chi_2):
        M_SI, dist_SI = None, None

        if self.units == 'dimensionless':
            raise ValueError('TODO: dimensionless EOB waveforms unsupported')
        elif self.units == 'mks':
            M_SI = M*Msuninkg
            dist_SI = dist*PC_SI*1e6
        else:
            raise ValueError('Unrecognized unit system:', self.units)

        m1_SI = M_SI/(1 + 1/q)
        m2_SI = m1_SI/q

        # Factor of 1.01 is for...fixing roundoff error in converting back and
        # forth between unit systems
        f_low = min(f_low, lalsim.EOBHighestInitialFreq(M*1.01))

        # This is a workaround for the situation that, for sufficiently low
        # masses, the Nyquist frequency ends up being lower than the ringdown
        # frequency for the highest mode.  EOB complains in this case, so
        # instead of passing along this exception, we'll ask for more samples,
        # and then only sample at the points we originally wanted.  There are
        # potential issues related to aliasing here, but we'll ignore them for
        # now, since they're most relevant for the ringdown, which is not the
        # most relevant part of the signal for the memory.
        skip = 1
        m1, m2 = m1_SI/Msuninkg, m2_SI/Msuninkg

        while True:
            try:
                lalsim.EOBCheckNyquistFrequency(m1, m2, chi_1, chi_2,
                                                self.ell_max, self.approx,
                                                du/skip)
            except RuntimeError:
                skip *= 2
                continue

            break

        try:
            packed = lalsim.SimInspiralChooseTDModes(0, du/skip, m1_SI, m2_SI,
                                                     *chi_1, *chi_2,
                                                     f_low, f_ref, dist_SI,
                                                     None, self.ell_max,
                                                     self.approx)
        except RuntimeError as err:
            print(M, dist, f_low, f_ref, du, q, chi_1, chi_2)
            raise err

        h_lms = {}

        while packed is not None:
            h_lms[packed.l, packed.m] = packed.mode.data.data[::skip]
            packed = packed.next

        u = np.linspace(0, du*len(h_lms[2, 2]), len(h_lms[2, 2]))

        return u, h_lms, True
