import gwsurrogate

from .generator import WaveformGenerator

class SurrogateGenerator(WaveformGenerator):

    def __init__(self, surrogate_model, **kwargs):
        super().__init__(**kwargs)
        self.surrogate = gwsurrogate.LoadSurrogate(surrogate_model)

    def _waveform(self, M, dist, f_low, f_ref, du, q,
                  *, chi_1, chi_2, **kwargs):

        if self.full_waveform:
            f_low = 0

        try:
            retval = self.surrogate(units='dimensionless', inclination=None,
                                    f_low=f_low*self.u_scale,
                                    f_ref=f_ref*self.u_scale,
                                    dt=(None if du is None
                                        else du/self.u_scale),
                                    chiA0=chi_1, chiB0=chi_2, q=q,
                                    **kwargs)[:-1]
        except Exception as err:
            print(M, dist, f_low, f_ref, du, q, chi_1, chi_2)
            raise err

        return retval[0], retval[1], False
