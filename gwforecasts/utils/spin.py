import math

def chi_eff(chi_1, chi_2, z_1, z_2, q):
    return (chi_1*z_1 + q*chi_2*z_2)/(1 + q)

def chi_p(chi_1, chi_2, z_1, z_2, q):
    chi_1_rho, chi_2_rho = (chi_1*math.sqrt(1 - z_1**2),
                            chi_2*math.sqrt(1 - z_2**2))

    return max(((4*q + 3)/(4 + 3*q))*q*chi_2_rho, chi_1_rho)

def cartesian(chi, z, chi_phi):
    return [chi*math.sqrt(1 - z**2)*math.cos(chi_phi),
            chi*math.sqrt(1 - z**2)*math.sin(chi_phi),
            chi*z]

def spherical(chi):
    mag = math.sqrt(chi[0]**2 + chi[1]**2 + chi[2]**2)
    z = chi[2]/mag
    phi = math.atan2(chi[1], chi[0])
    return [mag, z, phi]

def chi_p_cartesian(chi_1, chi_2, q):
    chi_1, z_1, unused = spherical(chi_1)
    chi_2, z_2, unused = spherical(chi_2)
    return chi_p(chi_1, chi_2, z_1, z_2, q)
