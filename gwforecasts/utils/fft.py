import math
import numpy as np

from . import misc

def window(signal, lpad, rpad, param):
    retval = np.concatenate((np.full(shape=lpad, fill_value=signal[0]), signal,
                             np.full(shape=rpad, fill_value=signal[-1])))

    if param == 0:
        return retval

    N = retval.shape[0]
    s = misc.smooth(np.arange(0, N), delta=param*N)

    return s*retval*np.flip(s)
