import json
import math

import numpy as np
from scipy.special import expit

def sign(s):
    if s % 2:
        return -1
    else:
        return 1

def smooth(x, x_0=0., delta=1.):
    retval = np.heaviside(x - x_0, 0)

    if delta == 0:
        return retval

    return retval*expit(delta/(delta + x_0 - np.minimum(x, delta + x_0))
                        + delta/(x_0 - np.maximum(x, x_0)))

def quantile(samples, q):
    factor = math.sqrt(len(samples)*q*(1 - q))
    quantiles = np.quantile(samples, q, axis=0)

    width = np.ptp(samples, axis=0)/len(samples)
    count = np.sum((quantiles - width/2 < samples)
                   & (samples < quantiles + width/2), axis=0)

    return quantiles, factor*width/count

class NDArrayEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()

        return json.JSONEncoder.default(self, obj)
