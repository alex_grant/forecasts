from cycler import cycler
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams.update({'font.size': 16,
                 'font.family': 'serif',
                 'font.serif': 'Latin Modern Roman'})

rcParams['text.usetex'] = True

rcParams['xtick.major.size'] = 7
rcParams['xtick.major.width'] = 1.5
rcParams['xtick.minor.size'] = 3
rcParams['xtick.minor.width'] = 1
rcParams['ytick.major.size'] = 7
rcParams['ytick.major.width'] = 1.5
rcParams['ytick.minor.size'] = 3
rcParams['ytick.minor.width'] = 1

rcParams['savefig.bbox'] = 'tight'
rcParams['savefig.pad_inches'] = 0

rcParams['figure.autolayout'] = True

custom_cycler = (cycler(color=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728']) +
                 cycler(linestyle=['-', '--', ':', '-.']))

plt.rc('axes', prop_cycle=custom_cycler)
