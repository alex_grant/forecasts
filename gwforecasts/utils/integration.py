import numpy as np
from scipy.integrate import cumulative_trapezoid

def def_integrate(y, x, a, b, mode):
    if b < a:
        return -def_integrate(y, x, b, a, mode)

    if mode == 'sum':
        dx = np.diff(x[a:b + 1])
        dx = np.concatenate(([0.], dx))
        return np.sum(y[a:b + 1]*dx)

    if mode == 'trapz':
        return np.trapz(y[a:b + 1], x=x[a:b + 1])

    raise NotImplementedError('Integration technique \'{0}\' '
                              'not supported'.format(mode))

def indef_integrate(y, x, start_idx, mode):
    if mode[0] == 'c':
        if mode == 'csum':
            dx = np.concatenate((np.array([0]), np.diff(x)))
            retval = np.cumsum(y*dx)

        if mode == 'ctrapz':
            retval = cumulative_trapezoid(y, x, initial=0.)

        if start_idx != 0:
            offset = retval[start_idx]
            retval -= offset

        return retval

    # This is slower, so should be avoided
    return np.array([def_integrate(y, x, start_idx, n, mode)
                     for n in range(0, len(x))])
