import math
import numpy as np
from py3nj import wigner3j
from gwtools import harmonics

from . import misc

def max_ell(C_lm):
    return max(C_lm.keys(), key=(lambda lm: lm[0]))[0]

def tensor(C_lm):
    C_E_lm, C_B_lm = {}, {}

    for l, m in C_lm.keys():
        C_E_lm[l, m] = (C_lm[l, m]
                        + misc.sign(m)*np.conj(C_lm[l, -m]))/math.sqrt(2)
        C_B_lm[l, m] = 1j*(C_lm[l, m]
                           - misc.sign(m)*np.conj(C_lm[l, -m]))/math.sqrt(2)

    return C_E_lm, C_B_lm

def swsh(C_E_lm, C_B_lm):
    C_lm = {}
    for l, m in C_E_lm.keys():
        C_lm[l, m] = (C_E_lm[l, m] - 1j*C_B_lm[l, m])/math.sqrt(2)

    return C_lm

def swsh_overlap(sp, spp, l, lp, lpp, mp, mpp):
    # TODO: determine if doing angular integrals (and then decomposing into
    # modes) is faster?

    prefactor = math.sqrt((2*l + 1)*(2*lp + 1)*(2*lpp + 1)/(4*math.pi))

    w1 = wigner3j(2*lp, 2*lpp, 2*l,
                  2*mp, 2*mpp, -2*(mp + mpp))

    w2 = wigner3j(2*lp, 2*lpp, 2*l,
                  -2*sp, -2*spp, 2*(sp + spp))

    return misc.sign(sp + spp + mp + mpp)*prefactor*w1*w2

def combine_modes(C_lm, theta, phi):
    retval = np.zeros_like(next(iter(C_lm.values())))

    for (l, m) in C_lm.keys():
        retval += harmonics.sYlm(-2, l, m, theta, phi)*C_lm[l, m]

    # $h_+$ and $h_\times$ (note the sign convention!)

    return np.real(retval), -np.imag(retval)
