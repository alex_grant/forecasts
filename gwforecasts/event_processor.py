import math
import multiprocessing
import json
# import cProfile
import itertools as it
import numpy as np

import gwtools as gwt

from .waveform_processor import WaveformProcessor
from .detectors.network import Network

class EventProcessor:

    def __init__(self, *, wp_config, network_configs, threshold_config,
                 fft_config, memories, fast_generator, slow_generator,
                 rerun=True, f_ref=None):

        self.wp = WaveformProcessor(**wp_config)

        self.networks = {}

        for name in network_configs:
            params = network_configs[name]
            self.networks[name] = Network(min_detectors=params['min_detectors'],
                                          network_config=params['config'])

        self.threshold = threshold_config

        self.null_window = {'pad': (0, 0), 'window': 0}

        if 'window' in fft_config:
            self.window_config = fft_config['window']
        else:
            self.window_config = self.null_window

        self.f_low, self.f_high = fft_config['f_range']

        self.memories = memories
        # Build caches of overlap functions *now*, so that they're shared
        # between the different processes
        for mem in self.memories:
            self.wp.add_memory(memory_type=mem, **self.memories[mem])
            self.wp.remove_memory(mem)

        self.fast_generator_params = fast_generator
        self.slow_generator_params = slow_generator

        self.rerun = rerun

        self.f_ref = f_ref

    def event_waveform(self, event, waveform_generator):
        self.wp.generator = waveform_generator


        self.wp.set_waveform(f_samp=2*self.f_high, f_low=self.f_low,
                             f_ref=self.f_ref/(event['M']*gwt.Msuninsec),
                             chi_1=event.get('chi_1', [0, 0, 0]),
                             chi_2=event.get('chi_2', [0, 0, 0]),
                             M=event['M'], dist=event['dist'], q=1/event['q'])

    def event_htildes(self, event, keys, window_config):
        return self.wp.freq_domain(inc=event['iota'], phi_ref=event['phi'],
                                   **window_config, keys=keys)

    def event_fast(self, event):
        for param in self.fast_generator_params[2]:
            low, hi = self.fast_generator_params[2][param]
            if not (low < event[param] < hi):
                return False

        return True

    def snr2_threshold(self, event, antenna_patterns, snr2s):
        if self.event_fast(event):
            self.event_waveform(event, self.fast_generator)
        else:
            self.event_waveform(event, self.slow_generator)

        key = self.threshold['key']
        cutoff = self.threshold['cutoff']
        plus_mod = self.threshold['antenna_plus']
        cross_mod = self.threshold['antenna_cross']

        flipped = event.copy()

        for param in self.threshold['event']:
            factor = self.threshold['event'][param].get('factor', 1)
            shift = self.threshold['event'][param].get('shift', 0)
            flipped[param] = factor*event[param] + shift

        freqs, htildes_true = self.event_htildes(event, [key],
                                                 self.null_window)
        _, htildes_flipped = self.event_htildes(flipped, [key],
                                                self.null_window)

        htildes_flipped[key] = (plus_mod*htildes_flipped[key][0],
                                cross_mod*htildes_flipped[key][1])

        def decompose(parity):
            true, flipped = htildes_true[key], htildes_flipped[key]
            return {key: ((true[0] + parity*flipped[0])/2,
                          (true[1] + parity*flipped[1])/2)}

        snr2s_odd = self.event_snr2s(freqs, decompose(-1), event,
                                     self.networks, antenna_patterns, [key])
        snr2s_even = self.event_snr2s(freqs, decompose(1), event,
                                      self.networks, antenna_patterns, [key])

        retval = {}

        for net in self.networks:
            snr2s[net][key + ' (odd)'] = snr2s_odd[net][key]
            snr2s[net][key + ' (even)'] = snr2s_even[net][key]

            retval[net] = snr2s[net][key + ' (odd)'] >= cutoff

        return retval

    def event_snr2s(self, freqs, htildes, event, nets, antenna_patterns, keys):
        snr2s = {}

        for net in nets:
            snr2s[net] = {}

            for key in keys:
                snr2s[net][key] = self.networks[net].snr2(freqs, htildes[key],
                                                          self.f_low,
                                                          self.f_high,
                                                          antenna_patterns[net],
                                                          event['T'])

        return snr2s

    def process(self, event, antenna_patterns):
        mem_snr2s = {net: {mem: 0. for mem in self.memories}
                     for net in self.networks}
        cut_snr2s = {net: {} for net in self.networks}

        thresholds = self.snr2_threshold(event, antenna_patterns, cut_snr2s)

        passing_nets = set()

        if self.memories:
            for net in self.networks:
                if thresholds[net]:
                    passing_nets.add(net)

        if passing_nets:
            if self.rerun:
                self.event_waveform(event, self.slow_generator)

            for mem in self.memories:
                self.wp.add_memory(memory_type=mem, **self.memories[mem])

            snr2s = self.event_snr2s(*self.event_htildes(event, self.memories,
                                                         self.window_config),
                                     event, passing_nets,
                                     antenna_patterns, self.memories)

            for net in passing_nets:
                mem_snr2s[net].update(snr2s[net])

            for mem in self.memories:
                self.wp.remove_memory(mem)

        event['mem_snr2s'] = mem_snr2s
        event['cut_snr2s'] = cut_snr2s
        return event

    def __call__(self, events):
        if not events:
            return []

        antenna_args = list(map(np.array, zip(*[[event['alpha'], event['delta'],
                                                 event['psi'], event['T']]
                                                for event in events])))

        antenna_patterns = {net: self.networks[net].load_antenna(*antenna_args)
                            for net in self.networks}

        self.wp.set_generator(self.fast_generator_params[0],
                              **self.fast_generator_params[1])
        self.fast_generator = self.wp.generator

        self.wp.set_generator(self.slow_generator_params[0],
                              **self.slow_generator_params[1])
        self.slow_generator = self.wp.generator

        # pr = cProfile.Profile()
        # pr.enable()

        retval = []

        for i, event in enumerate(events):
            antenna = {net: {det: [(F_p[i], F_c[i])
                                   for F_p, F_c in antenna_patterns[net][det]]
                             for det in antenna_patterns[net]}
                       for net in self.networks}

            retval.append(self.process(event, antenna))

        # pr.disable()
        # pr.print_stats(sort='tottime')
        return retval

def multiprocess(events_path, full_config, jobs=8):
    processed_events = []
    event_cuts = full_config.pop('event_cuts')

    with open(events_path, 'r') as file:
        events = json.load(file)
        for event in events:
            for key, (low, high) in event_cuts.items():
                if key == 'M_per_dist':
                    if low > event['M']/event['dist']:
                        break
                elif low > event[key] or high < event[key]:
                    break
            else:
                processed_events.append(event)

    print('Total events passing event cut:', len(processed_events))

    job_events = [[] for i in range(jobs)]

    for i, event in enumerate(processed_events):
        job_events[i % jobs].append(event)

    pool = multiprocessing.Pool(jobs)
    proc = EventProcessor(**full_config)

    job_results = pool.map(proc, job_events)

    combined_results = list(it.chain.from_iterable(job_results))

    combined_results.sort(key=(lambda event: event['T']))

    return combined_results
