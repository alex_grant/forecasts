import math
import numpy as np
import itertools as it

from .memories.displacement import DisplacementMemory
from .memories.displacement_test import DisplacementMemoryTest
from .memories.spin import SpinMemory
from .memories.com import CoMMemory
from .utils import misc, harmonics, fft
from .waveforms.surrogate import SurrogateGenerator
from .waveforms.eob import EOBGenerator

class WaveformProcessor:

    def __init__(self, *, ell_max, hkey='Waveform'):
        self.surrogate = None
        self.hkey = hkey
        self.h_lms = {hkey : None}
        self.u = None
        self.memories = {}
        self.overlap_cache = {}
        self.ell_max = ell_max

    def cache_overlaps(self, *spins):
        for sp, spp in spins:
            if (sp, spp) in self.overlap_cache.keys():
                return

            self.overlap_cache[sp, spp] = {}

            for l, lp, lpp in it.product(range(2, self.ell_max + 1), repeat=3):
                tmp = {}
                for mp, mpp in it.product(range(-lp, lp + 1),
                                          range(-lpp, lpp + 1)):
                    if np.abs(mp + mpp) <= l:
                        tmp[mp, mpp] = harmonics.swsh_overlap(sp, spp, l,
                                                              lp, lpp, mp, mpp)
                self.overlap_cache[sp, spp][l, lp, lpp] = tmp

    def add_memory(self, memory=None, memory_type=None, key=None, **kwargs):
        if memory is not None:
            if memory_type is not None:
                raise ValueError('Must not specify memory type')

            if key is None:
                raise ValueError('Must specify key')

        else:
            if key is None:
                key = memory_type

            if memory_type == 'Displacement':
                self.cache_overlaps((-2, 2))
                memory = DisplacementMemory(**kwargs, ell_max=self.ell_max,
                                            overlap_cache=self.overlap_cache)
            elif memory_type == 'Spin':
                self.cache_overlaps((-3, 2), (-2, 1))
                memory = SpinMemory(**kwargs, ell_max=self.ell_max,
                                    overlap_cache=self.overlap_cache)
            elif memory_type == 'Center-of-Mass':
                self.cache_overlaps((-3, 2), (-2, 1))
                memory = CoMMemory(**kwargs, ell_max=self.ell_max,
                                   overlap_cache=self.overlap_cache)
            elif memory_type == 'Displacement (Test)':
                memory = DisplacementMemoryTest(**kwargs)
            else:
                raise ValueError('Unrecognized memory type: '
                                 '{0}'.format(memory_type))

        # Generate the memories if the surrogate waveform already exists.
        if self.h_lms[self.hkey] is not None:
            self.h_lms[key] = memory.process(self.h_lms[self.hkey], self.u)
        else:
            self.h_lms[key] = None

        self.memories[key] = memory

    def remove_memory(self, key):
        del self.h_lms[key]
        del self.memories[key]

    def set_generator(self, model, **kwargs):
        self.generator = None

        if model[:2] == 'NR':
            self.generator = SurrogateGenerator(model, ell_max=self.ell_max,
                                                **kwargs)
        elif model[:3] == 'EOB' or model[:4] == 'SEOB':
            self.generator = EOBGenerator(model, ell_max=self.ell_max, **kwargs)
        else:
            raise ValueError('Waveform model', model, 'not supported')

    def set_waveform(self, **kwargs):
        self.u, self.h_lms[self.hkey] = self.generator.waveform(**kwargs)

        # Generate the memories.  If you're wondering why this function takes so
        # long, this is probably why...
        for key in self.h_lms.keys():
            if key != self.hkey:
                memory = self.memories[key]
                self.h_lms[key] = memory.process(self.h_lms[self.hkey], self.u)

    def time_domain(self, inc=None, phi_ref=0., keys=None):
        retval_u, retval_h = self.generator.rescale_u(self.u), {}

        if keys is None:
            keys = self.h_lms.keys()

        for key in keys:
            retval_h[key] = self.generator.rescale_h(self.h_lms[key])

        if inc != None:
            for key in keys:
                retval_h[key] = harmonics.combine_modes(retval_h[key],
                                                        inc, math.pi - phi_ref)

        return retval_u, retval_h

    def pad_and_fft(self, u, h, pad, window, fraction=True,
                    return_windowed=False):
        if not fraction:
            length = u[-1] - u[0]
            pad = pad[0]/length, pad[1]/length
            window = window/(length*(1 + pad[0] + pad[1]))

        lpad = math.floor(u.shape[0]*pad[0])
        rpad = math.floor(u.shape[0]*pad[1])

        du = u[1] - u[0]
        freqs, htilde = np.fft.rfftfreq(u.shape[0] + lpad + rpad, d=du), {}

        norm = 'backward'

        u = np.concatenate((np.arange(-lpad, 0)*du + u[0], u,
                            np.arange(0, rpad)*du + u[-1] + du))

        for key in h.keys():
            if isinstance(h[key], dict):
                htilde[key] = {}
                for lm in h[key].keys():
                    h[key][lm] = fft.window(h[key][lm], lpad, rpad, window)
                    htilde[key][lm] = np.fft.rfft(h[key][lm], norm=norm)*du

            else:
                h[key] = (fft.window(h[key][0], lpad, rpad, window),
                          fft.window(h[key][1], lpad, rpad, window))
                htilde[key] = (np.fft.rfft(h[key][0], norm=norm)*du,
                               np.fft.rfft(h[key][1], norm=norm)*du)

        if return_windowed:
            return freqs, htilde, u, h
        else:
            return freqs, htilde

    def freq_domain(self, pad=(0, 0), window=0., fraction=True,
                    return_windowed=False, keys=None, **kwargs):
        u, h = self.time_domain(keys=keys, **kwargs)

        return self.pad_and_fft(u, h, pad=pad, window=window, fraction=fraction,
                                return_windowed=return_windowed)
