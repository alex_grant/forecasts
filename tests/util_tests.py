#!/usr/bin/env python3

import math
import itertools

from gwforecasts.utils import harmonics, misc

def overlap_subtests(s, sp, lpp, l, lp, m, mp):
    ssp_C_lpp_llpmmp = harmonics.swsh_overlap(s, sp, lpp, l, lp, m, mp)
    sign = misc.sign(l + lp + lpp)
    err = False
    if not math.isclose(ssp_C_lpp_llpmmp,
                        sign*harmonics.swsh_overlap(-s, -sp, lpp, l, lp, m, mp),
                        abs_tol=1e-15):
        print("Sign negation error:", ssp_C_lpp_llpmmp, "vs",
              sign*harmonics.swsh_overlap(-s, -sp, lpp, l, lp, m, mp))
        err = True
    if not math.isclose(ssp_C_lpp_llpmmp,
                        sign*harmonics.swsh_overlap(s, sp, lpp, l, lp, -m, -mp),
                        abs_tol=1e-15):
        print("m negation error:", ssp_C_lpp_llpmmp, "vs",
              sign*harmonics.swsh_overlap(s, sp, lpp, l, lp, -m, -mp))
        err = True
    if not math.isclose(ssp_C_lpp_llpmmp,
                        harmonics.swsh_overlap(sp, s, lpp, lp, l, mp, m),
                        abs_tol=1e-15):
        print("Swap error:", ssp_C_lpp_llpmmp, "vs",
              harmonics.swsh_overlap(sp, s, lpp, lp, l, mp, m))
        err = True

    if err:
        print("l, l' =", (l, lp),
              "\nm, m' =", (m, mp), "\ns, s' =", (s, sp),
              "\nl'' =", lpp)

def overlap_test(max_ell):
    for l, lp in itertools.product(range(0, max_ell + 1),
                                   range(0, max_ell + 1)):
        for m, mp in itertools.product(range(-l, l + 1),
                                       range(-lp, lp + 1)):
            for s, sp in itertools.product(range(-l, l + 1),
                                           range(-lp, lp + 1)):
                for lpp in range(max(abs(m + mp), abs(s + sp), abs(l - lp)),
                                 l + lp + 1):
                    overlap_subtests(s, sp, lpp, l, lp, m, mp)

overlap_test(4)
