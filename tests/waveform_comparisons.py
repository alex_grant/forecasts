#!/usr/bin/env python3

import argparse

import numpy as np
import matplotlib.pyplot as plt
import gwtools as gwt
import h5py

from gwforecasts.waveform_processor import WaveformProcessor
from gwforecasts.detectors.network import Network
from gwforecasts.utils.harmonics import combine_modes

wp = WaveformProcessor(ell_max=5)

wp.set_generator('SEOBNRv4PHM', units='mks')
slow_generator = wp.generator
wp.set_generator('NRSur7dq4', units='mks')
fast_generator = wp.generator

f_low = 10
f_samp = 8192

# SXS:BBH:0362
M = 50
dist = 1000
q = 0.5
chi_1 = np.array([-0.0301730241025, 0.197706194037, 0.00228306950755])
chi_2 = np.array([-0.0261930106343, 0.00386232088568, -0.799141471505])

inc = 0.08276490630322134
phi_ref = 4.6157870941817345
nr_phi_off = 4.398951

f_ref = 0.0164/(np.pi*M*gwt.Msuninsec)

phi_flipped = phi_ref + np.pi/2

wp.generator = fast_generator

wp.set_waveform(M=M, dist=dist,
                f_samp=f_samp, f_ref=f_ref, f_low=0,
                q=q, chi_1=chi_1, chi_2=chi_2)

u_sur, h_true_sur = wp.time_domain(inc=inc, phi_ref=phi_ref)
_, h_flipped_sur = wp.time_domain(inc=inc, phi_ref=phi_flipped)

wp.generator = slow_generator

wp.set_waveform(M=M, dist=dist,
                f_samp=f_samp, f_ref=f_ref, f_low=f_low,
                q=q, chi_1=chi_1, chi_2=chi_2)

u_eob, h_true_eob = wp.time_domain(inc=inc, phi_ref=phi_ref)
_, h_flipped_eob = wp.time_domain(inc=inc, phi_ref=phi_flipped)

key = 'Waveform'

eob_max = u_eob[np.argmax(h_true_eob[key][0])]

# Adapted from https://github.com/sxs-collaboration/catalog_tools/blob/master/Examples/waveform_tutorial.ipynb
def read_nr(file):
    data = h5py.File(file, 'r')
    h_lms = {}
    modes = sorted(data[sorted(data)[0]])
    for mode in modes:
        if mode.startswith('Y'):
            if mode[6] == '-':
                lm = int(mode[3]), int(mode[6:8])
            else:
                lm = int(mode[3]), int(mode[6])

            idx = 'Extrapolated_N4.dir/Y_l{0}_m{1}.dat'.format(*lm)
            h_lms[lm] = slow_generator.h_scale*(data[idx][:, 1]
                                                + 1j*data[idx][:, 2])

    u = data[idx][:, 0]*slow_generator.u_scale

    return (u, {'Waveform': combine_modes(h_lms, inc, phi_ref + nr_phi_off)},
            {'Waveform': combine_modes(h_lms, inc, phi_flipped + nr_phi_off)})

u_nr, h_true_nr, h_flipped_nr = read_nr('../../rhOverM_Asymptotic_GeometricUnits_CoM.h5')

u_nr_max = u_nr[np.argmax(h_true_nr[key][0])]

def odd_amplitude(waveform, waveform_flipped):
    return np.sqrt((waveform[key][0] + waveform_flipped[key][0])**2 +
                   (waveform[key][1] + waveform_flipped[key][1])**2)/2

def odd_plus(waveform, waveform_flipped):
    return (waveform[key][0] + waveform_flipped[key][0])/2

plt.figure('Odd waveform')

plt.plot(u_sur, odd_amplitude(h_true_sur, h_flipped_sur),
         label='Surrogate')
plt.plot(u_eob - eob_max, odd_amplitude(h_true_eob, h_flipped_eob),
         label='EOB')
plt.plot(u_nr - u_nr_max, odd_amplitude(h_true_nr, h_flipped_nr),
         label='Numerical Relativity')

plt.legend()

plt.show()
