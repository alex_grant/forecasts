#!/usr/bin/env python3

import time
import argparse
import json

parser = argparse.ArgumentParser(description='Process a series of events, ' \
                                 'generating SNR as a function of time')
parser.add_argument('-j', '--jobs', help='Number of subprocesses to spawn',
                    default=8, type=int)
parser.add_argument('config', help='Config file')
parser.add_argument('event_sequence', help='Sequence of events')
parser.add_argument('output', help='Output SNRs as functions of time')

args = parser.parse_args()

import gwforecasts.event_processor as event_processor

config_path = args.config
config = {}

with open(config_path, 'r') as config_file:
    config = json.load(config_file)

event_path = args.event_sequence

start = time.time()
events = event_processor.multiprocess(event_path, config, jobs=args.jobs)

print("Time elapsed for {0}: {1}".format(event_path, time.time() - start))

output_path = args.output

with open(output_path, 'w') as output_file:
    json.dump(events, output_file)
