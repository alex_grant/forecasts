#!/usr/bin/env python3

import argparse
import json

parser = argparse.ArgumentParser(description='Compare two sets of events ' \
                                 'using the Kolmogorov-Smirnov test')
parser.add_argument('events', nargs=2,
                    help='Two sets of events to compare')
parser.add_argument('-p', '--plot', action='store_true',
                    help='Plot the distributions to allow comparison by eye')

args = parser.parse_args()

import numpy as np
from scipy.stats import ks_2samp
import matplotlib.pyplot as plt

import gwforecasts.utils.plot

event_lists = [{}, {}]

for i, filename in enumerate(args.events):
    with open(filename, 'r') as f:
        full_list = json.load(f)

        for key in full_list[0]:
            if not np.isscalar(full_list[0][key]):
                for j in range(0, len(full_list[0][key])):
                    event_lists[i][key + str(j)] = []
            else:
                event_lists[i][key] = []

        for event_dict in full_list:
            for key in event_dict:
                if not np.isscalar(event_dict[key]):
                    for j in range(0, len(event_dict[key])):
                        event_lists[i][key + str(j)].append(event_dict[key][j])
                else:
                    event_lists[i][key].append(event_dict[key])

        for key in event_lists[i]:
            event_lists[i][key] = np.array(event_lists[i][key])


for key in event_lists[0]:
    print(key)
    print(ks_2samp(event_lists[0][key], event_lists[1][key]))
    print('')

if args.plot:
    for key in event_lists[0]:
        plt.figure(key)
        for i in (0, 1):
            plt.hist(event_lists[i][key], bins=100, label=args.events[i],
                     alpha=0.3)
            plt.legend()


    plt.show()
