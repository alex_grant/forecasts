#!/usr/bin/env python3

import argparse
import json

parser = argparse.ArgumentParser(description='Generate summary statistics ' \
                                 'for a set of SNR files')
parser.add_argument('-i', '--interpolation-points', default=10000, type=int,
                    help='Number of interpolation points')
parser.add_argument('-s', '--symmetric', help='Symmetric interval to use for '
                    'upper and lower percentiles', default=68, type=float)
parser.add_argument('-c', '--cut', nargs=3, metavar=('VAR', 'MIN', 'MAX'),
                    help='Variable to use for a cutoff, along with a range')
parser.add_argument('-n', '--number', action='store_true',
                    help='Print number of events that pass cuts')
parser.add_argument('-m', '--memories', nargs='+',
                    help='Memory effects to include')
parser.add_argument('-f', '--final', choices=['min', 'max'], default=None,
                    help=('Only store final value of the SNR, along with either'
                          'the minimum or maximum cutoff (note that this'
                          '*overwrites* part of the output file!)'))
parser.add_argument('-t', '--threshold', type=float, default=0,
                    help='Odd part of waveform threshold')
parser.add_argument('-e', '--even-threshold', action='store_true',
                    help='Set the threshold to be the even threshold')
parser.add_argument('-o', '--output', required=True,
                    help='Output file')
parser.add_argument('events', nargs='+', help='Event files containing SNR info')

args = parser.parse_args()

import numpy as np

import gwforecasts.utils.misc as misc

Ts = []
snrs = []

if args.number:
    num_events = []

for path in args.events:
    with open(path, 'r') as file:
        Ts.append([])
        snrs.append({})

        events = json.load(file)

        for net in events[0]['mem_snr2s']:
            if args.memories is None:
                keys = events[0]['mem_snr2s'][net]
            else:
                keys = args.memories

            snrs[-1][net] = {key: [] for key in keys}

        for event in events:
            if (args.cut is not None
                and (float(args.cut[1]) > event[args.cut[0]]
                     or event[args.cut[0]] > float(args.cut[2]))):
                continue

            Ts[-1].append(event['T'])

            for net in snrs[-1]:
                if args.threshold != 0.:
                    cut = 'odd' if not args.even_threshold else 'even'
                    if (event['cut_snr2s'][net]['Waveform (' + cut + ')']
                        < args.threshold):
                        for key in snrs[-1][net]:
                            snrs[-1][net][key].append(0.)
                        continue

                for key in snrs[-1][net]:
                    snrs[-1][net][key].append(event['mem_snr2s'][net][key])

        for net in snrs[-1]:
            for key in snrs[-1][net]:
                tmp = np.cumsum(snrs[-1][net][key])
                snrs[-1][net][key] = np.sqrt(tmp)

        if args.number:
            num_events.append(len(Ts[-1]))

for i, T_list in enumerate(Ts):
    if len(T_list) == 0:
        continue

    T_interp = np.linspace(T_list[0], T_list[-1], num=args.interpolation_points)
    break

snr_median = {net: {} for net in snrs[0]}
snr_lower = {net: {} for net in snrs[0]}
snr_upper = {net: {} for net in snrs[0]}

for net in snrs[0]:
    for key in snrs[0][net]:
        snrs_interp = []
        for T, snr in zip(Ts, snrs):
            snrs_interp.append(np.interp(T_interp, T, snr[net][key]))

        snr_median[net][key] = misc.quantile(snrs_interp, 1/2)
        snr_lower[net][key] = misc.quantile(snrs_interp,
                                            (1 - args.symmetric*0.01)/2)
        snr_upper[net][key] = misc.quantile(snrs_interp,
                                            (1 + args.symmetric*0.01)/2)

if args.final is None:
    with open(args.output, 'w') as out:
        json.dump({'T': T_interp,
                   'median': snr_median,
                   'lower': snr_lower,
                   'upper': snr_upper}, out,
                  cls=misc.NDArrayEncoder)

else:
    def final_value(snr_dict):
        return {net: {key: [val[-1] for val in snr_dict[net][key]]
                      for key in snr_dict[net]}
                for net in snr_dict}

    try:
        with open(args.output, 'r') as out:
            data = json.load(out)
    except FileNotFoundError:
        data = {}

    if args.final == 'min':
        key = args.cut[1]
    else:
        key = args.cut[2]

    data[key] = {'median': final_value(snr_median),
                 'lower': final_value(snr_lower),
                 'upper': final_value(snr_upper)}

    with open(args.output, 'w') as out:
        json.dump(data, out)

if args.number:
    print('Median number of events:', np.median(num_events))
