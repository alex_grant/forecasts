#!/usr/bin/env python3

import argparse
import json
import math
from multiprocessing import Pool
from pathlib import Path

parser = argparse.ArgumentParser(description='Generate a series of events')
limits = parser.add_mutually_exclusive_group(required=True)
limits.add_argument('-t', '--t-max', help='Maximum time in years', type=float,
                    default=None)
limits.add_argument('-N', '--n-max', type=int, default=None,
                    help='Maximum number of events to draw')
parser.add_argument('-n', '--batch-size', type=float, default=math.inf,
                    help='Maximum number of events to generate at a time')
parser.add_argument('-w', '--walkers', type=int, default=80,
                    help='Number of Markov chain walkers')
parser.add_argument('-j', '--jobs', help='Number of jobs', type=int, default=8)
parser.add_argument('-c', '--cosmology', help='Cosmological model',
                    default='Planck18')
parser.add_argument('-p', '--population', required=True,
                    help='Population file, or directory if -d is passed')
parser.add_argument('-d', '--directory', action='store_true',
                    help='Treat POPULATION and output as directories')
parser.add_argument('output', nargs='+',
                    help='List of output files, or directories if -d is passed')

args = parser.parse_args()

import astropy.cosmology as cosmo
import numpy as np

from gwforecasts.populations.event import EventSampler
import gwforecasts.populations.angles as angles
import gwforecasts.populations.primary as primary
import gwforecasts.populations.mass_ratio as mass_ratio
import gwforecasts.populations.mass as mass
import gwforecasts.populations.rate as rate
import gwforecasts.populations.spin as spin

def generate_events(input_path, output_paths):

    with open(input_path, 'r') as input_file:
        pop_dict = json.load(input_file)

        sampler_dict = pop_dict['samplers']

        sampler_list = [angles.Uniform]

        for module in sampler_dict:
            sampler_list.append(getattr(globals()[module],
                                        sampler_dict[module]))

        sampler = EventSampler(sampler_classes=sampler_list,
                               walkers=args.walkers, max_events=args.batch_size,
                               cosmology=getattr(cosmo, args.cosmology),
                               z_samples=1000, **pop_dict['params'])

        for output_path in output_paths:
            with open(output_path, 'w') as output_file:
                json.dump(sampler.draw_events(T_max=args.t_max,
                                              N_max=args.n_max), output_file)

    return

p = Pool(args.jobs)

if not args.directory:
    input_path = Path(args.population)

    arg_list = []

    for i, out in enumerate(args.output):
        if i < args.jobs:
            arg_list.append((input_path, [Path(out)]))
        else:
            arg_list[i % args.jobs][1].append(Path(out))

else:
    arg_list = [(path, [(dir/(path.stem + '_{0}.json'.format(i))
                         if len(args.output) > 1
                         else dir/path.name)
                        for i, dir in enumerate(map(Path, args.output))])
                for path in Path(args.population).iterdir()]

p.starmap(generate_events, arg_list)
