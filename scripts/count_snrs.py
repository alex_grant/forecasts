#!/usr/bin/env python3

import argparse
import json

parser = argparse.ArgumentParser(description='Count the number of events ' \
                                 'satisfying certain criteria')
parser.add_argument('-s', '--symmetric', help='Symmetric interval to use for '
                    'upper and lower percentiles', default=68, type=float)
parser.add_argument('-c', '--cut', nargs=3, metavar=('VAR', 'MIN', 'MAX'),
                    help='Variable to use for a cutoff, along with a range')
parser.add_argument('-t', '--threshold', type=float, default=0,
                    help='Odd part threshold')
parser.add_argument('-e', '--even-threshold', action='store_true',
                    help='Set the threshold to be the even threshold')
parser.add_argument('-o', '--output', required=True,
                    help='Output file')
parser.add_argument('events', nargs='+', help='Event files containing SNR info')

args = parser.parse_args()

import numpy as np

import gwforecasts.utils.misc as misc

counts = {}

for i, snr_file in enumerate(args.events):
    with open(snr_file, 'r') as f:
        events = json.load(f)

    for event in events:
        if (args.cut is not None
            and (float(args.cut[1]) > event[args.cut[0]]
                 or event[args.cut[0]] > float(args.cut[2]))):
            continue

        for net in event['mem_snr2s']:
            if net not in counts:
                counts[net] = [0]*len(args.events)

            cut = 'odd' if not args.even_threshold else 'even'
            if (event['cut_snr2s'][net]['Waveform (' + cut + ')']
                >= args.threshold):
                counts[net][i] += 1

medians = {}
lowers = {}
uppers = {}

for net in counts:
    medians[net], _ = misc.quantile(counts[net], 1/2)
    lowers[net], _ = misc.quantile(counts[net], (1 - 0.01*args.symmetric)/2)
    uppers[net], _ = misc.quantile(counts[net], (1 + 0.01*args.symmetric)/2)

with open(args.output, 'w') as out:
    json.dump({net: {'median': medians[net],
                     'minus': medians[net] - lowers[net],
                     'plus': uppers[net] - medians[net]}
               for net in medians}, out)
