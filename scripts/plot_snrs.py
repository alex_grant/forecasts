#!/usr/bin/env python3

import argparse
import json

parser = argparse.ArgumentParser(description='Plot summary statistics of SNRs')
parser.add_argument('-p', '--plot', default='all',
                    choices=['median', 'bounds', 'all'],
                    help='Which curves to plot (default: all)')
parser.add_argument('-f', '--fit', default=None,
                    choices=['median', 'bounds', 'all'],
                    help='Which curve to fit (default: none of them)')
parser.add_argument('-e', '--errors', action='store_true',
                    help='Plot error bars for all curves which are fit')
parser.add_argument('-E', '--force-errors', action='store_true',
                    help='Plot error bars for all curves')
parser.add_argument('-F', '--fit-func', default='sqrt',
                    choices=['sqrt', 'sqrt_offset', 'pow', 'pow_offset'],
                    help='Choice of function to fit')
parser.add_argument('-S', '--fit-start', default=0., type=float,
                    help='Time (in years) to start fitting, once the curve is '
                    'non-zero')
parser.add_argument('-t', '--target-snr', default=None, type=float,
                    help='Plot target SNR')
parser.add_argument('-n', '--net', default=None, nargs='+',
                    help='Detector networks to plot')
parser.add_argument('-o', '--output', help='Output files', default=None,
                    nargs='+')
parser.add_argument('snrs', nargs='+',
                    help='Files containing summary statistics (median and '
                    'lower and upper quantiles) of the SNRs of a population')

args = parser.parse_args()

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

import gwforecasts.utils.plot

Ts = []

medians = []
lowers = []
uppers = []

for path in args.snrs:
    with open(path, 'r') as file:
        snrs = json.load(file)

        Ts.append(snrs['T'])
        medians.append(snrs['median'])
        lowers.append(snrs['lower'])
        uppers.append(snrs['upper'])

def plot_memory(key):
    prop_cycler = plt.rcParams['axes.prop_cycle']()

    if args.fit:
        print(key + ':\n')

    if args.net:
        nets = args.net
    else:
        nets = medians[0]

    for net in nets:
        if args.fit:
            print(net + ':\n')

        for T, median, lower, upper, path in zip(Ts, medians, lowers, uppers,
                                                 args.snrs):
            prop = next(prop_cycler)

            if len(args.snrs) == 1:
                label = net
            else:
                label = '{0}: {1}'.format(path, net)

            median = median[net][key]
            lower = lower[net][key]
            upper = upper[net][key]
            if args.plot in ('median', 'all'):
                if ((args.errors and args.fit in ('median', 'all'))
                    or args.force_errors):
                    plt.errorbar(T, median[0], yerr=median[1], label=label,
                                 color=prop['color'], linestyle='')
                else:
                    plt.plot(T, median[0], label=label, color=prop['color'],
                             linestyle=prop['linestyle'])

            if args.plot in ('bounds', 'all'):
                plt.fill_between(T, lower[0], upper[0], alpha=0.3,
                                 color=prop['color'])
                if ((args.errors and args.fit in ('bounds', 'all'))
                    or args.force_errors):
                    for y in (lower, upper):
                        plt.errorbar(T, y[0], yerr=y[1], linestyle='',
                                     color=prop['color'])

            if args.fit:
                print(path + ':\n')

                curves = []

                if args.fit in ('median', 'all'):
                    curves.append((median, 'Median'))

                if args.fit in ('bounds', 'all'):
                    curves.append((lower, 'Lower quantile'))
                    curves.append((upper, 'Upper quantile'))

                prop = next(prop_cycler)

                for curve, name in curves:
                    fit_curve(T, curve, name, prop)

    if args.target_snr:
        prop = next(prop_cycler)
        plt.axhline(y=args.target_snr, color=prop['color'],
                    linestyle=prop['linestyle'])

    plt.xlabel('$T$ (yr)')

    ylabel = key.lower()[:4]
    if len(key) > 4:
        ylabel += '.'

    plt.ylabel('Effective $\\rho_{{h^{{\\rm {0}}}}}$'.format(ylabel))

    plt.legend()

def fit_curve(T, curve, name, prop):
    idx = max(np.searchsorted(curve[0], 0, side='right') - 1, 0)
    T_0 = T[idx]

    fit_idx = np.searchsorted(T, T_0 + args.fit_start, side='left')

    T_trunc = np.array(T[fit_idx:])
    curve_trunc = curve[0][fit_idx:]
    err_trunc = curve[1][fit_idx:]

    if args.fit_func == 'sqrt':
        f = lambda t, alpha: alpha*(t - T_0)**(1/2)

        param_names = ['alpha']
    elif args.fit_func == 'sqrt_offset':
        f = lambda t, alpha, c: alpha*(t - T_0)**(1/2) + c

        param_names = ['alpha', 'c']
    elif args.fit_func == 'pow':
        f = lambda t, alpha, beta: alpha*(t - T_0)**beta

        param_names = ['alpha', 'beta']
    elif args.fit_func == 'pow_offset':
        f = lambda t, alpha, beta, c: alpha*(t - T_0)**beta + c

        param_names = ['alpha', 'beta', 'c']

    params, params_cov = curve_fit(f, T_trunc, curve_trunc, sigma=err_trunc,
                                   absolute_sigma=True)

    residual = curve_trunc - f(T_trunc, *params)
    chisq = np.sum((residual/err_trunc)**2)/(len(T_trunc) - len(params))

    plt.plot(T_trunc, f(T_trunc, *params), linestyle=prop['linestyle'],
             color=prop['color'])

    print(name + ':\n')
    print('chi^2:', chisq)
    for param, err, name in zip(params, np.sqrt(np.diag(params_cov)),
                                param_names):
        print('{0}: {1} +/- {2}'.format(name, param, err))

    print('')

first_net = next(iter(medians[0]))

for i, key in enumerate(medians[0][first_net]):
    plt.figure()
    plt.minorticks_on()
    plt.autoscale(tight=True)
    plot_memory(key)
    if args.output is not None:
        plt.savefig(args.output[i])

if args.output is None:
    plt.show()
