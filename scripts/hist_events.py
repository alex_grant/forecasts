#!/usr/bin/env python3

import argparse
import json
from pathlib import Path

parser = argparse.ArgumentParser(description='Plot a histogram of events by ' \
                                 'some parameter')
parser.add_argument('-d', '--directory', help='Treat EVENTS as directories',
                    action='store_true')
parser.add_argument('-D', '--density', help='Plot histograms as densities',
                    action='store_true')
parser.add_argument('-r', '--range', help='Range of values of the parameter',
                    nargs=2, type=float, default=None)
parser.add_argument('param', help='Parameter to histogram')
parser.add_argument('events', nargs='+', help='Lists of events to histogram')

args = parser.parse_args()

import matplotlib.pyplot as plt

color_cycler = plt.rcParams['axes.prop_cycle']()

for name in args.events:
    param_values = []
    for path in (Path(name).iterdir() if args.directory else [Path(name)]):
        with open(path, 'r') as file:
            events = json.load(file)
            for event in events:
                if args.param == 'M_per_dist':
                    param_values.append(event['M']/event['dist'])
                else:
                    param_values.append(event[args.param])

    color = next(color_cycler)['color']

    plt.hist(param_values, bins=100, range=args.range,
             label=name, density=args.density,
             color=color, alpha=0.3)

plt.legend()
plt.show()
