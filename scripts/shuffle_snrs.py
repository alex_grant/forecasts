#!/usr/bin/env python3

import argparse
import json
import math
import random

parser = argparse.ArgumentParser(description='Draw a shuffled set of SNRs ' \
                                 'from some given set, allowing for ' \
                                 'extrapolation in time')
parser.add_argument('-t', '--time', help='Maximum time to draw for',
                    type=float, default=None)
parser.add_argument('-i', '--initial', help='Starting time to draw from',
                    type=float, default=0.)
parser.add_argument('-f', '--final', help='Final time to draw from',
                    type=float, default=math.inf)
parser.add_argument('-c', '--cut', nargs=3, metavar=('VAR', 'MIN', 'MAX'),
                    help='Variable to use for a cutoff, along with a range')
parser.add_argument('snr2', help='Set of SNR^2s to draw from')
parser.add_argument('out', help='Output')

args = parser.parse_args()

with open(args.snr2, 'r') as snr2_file:
    events = json.load(snr2_file)

T = 0.

pool = []

for event in events:
    if event['T'] < args.initial or event['T'] > args.final:
        continue

    event['dt'] = event['T'] - T
    T = event['T']
    del event['T']
    pool.append(event)

if args.time is None:
    T_max = T
else:
    T_max = args.time

T = 0.

events = []

while T < T_max:
    event = random.choice(pool).copy()
    T += event['dt']
    event['T'] = T
    del event['dt']

    if (args.cut is not None
        and (float(args.cut[1]) > event[args.cut[0]]
             or event[args.cut[0]] > float(args.cut[2]))):
        continue

    events.append(event)

with open(args.out, 'w') as out_file:
    json.dump(events, out_file)
