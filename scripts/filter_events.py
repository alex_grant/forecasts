#!/usr/bin/env python3

import argparse
import json

parser = argparse.ArgumentParser(description='Filter events by the value of ' \
                                 'some variable')
parser.add_argument('-c', '--cut', nargs=3, metavar=('VAR', 'MIN', 'MAX'),
                    help='Variable to use for a cutoff, along with a range',
                    required=True)
parser.add_argument('input', help='File containing events')
parser.add_argument('output', help='Output file')

args = parser.parse_args()

import ijson

events = []

with open(args.input, 'rb') as in_file:
    for event in ijson.items(in_file, 'item', use_float=True):
        if (float(args.cut[1]) <= event[args.cut[0]]
            and event[args.cut[0]] <= float(args.cut[2])):
            events.append(event)

with open(args.output, 'w') as out_file:
    json.dump(events, out_file)
