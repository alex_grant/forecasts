#!/usr/bin/env python3

import sys
import argparse
import json

parser = argparse.ArgumentParser(description='Generate a series of populations')
parser.add_argument("-m", "--mass-model", help="Model type", default='C',
                    choices=['A', 'B', 'C', 'D', 'E'])
parser.add_argument("-g", "--gwtc", help="Version of GWTC", default='2',
                    choices=['1', '2'])
parser.add_argument("-r", "--evolve-redshift", action="store_true",
                    help="Use an evolving redshift model")
parser.add_argument("-s", "--spinless", action="store_true",
                    help="Do not generate a spin population")
parser.add_argument("-z", "--max-redshift", type=float, default=2.3)
parser.add_argument("-j", "--jobs", help="Number of jobs", type=int, default=8)
parser.add_argument("input", help="Input file")
parser.add_argument("output", nargs="+", help='Output file(s)')

args = parser.parse_args()

from gwforecasts.populations.population import ModelSampler

sampler = ModelSampler(filename=args.input, model=args.mass_model,
                       release=('GWTC-' + args.gwtc),
                       z_max=args.max_redshift,
                       include_spins=(not args.spinless),
                       evolve_redshift=args.evolve_redshift,
                       processes=args.jobs)

samples = sampler.draw(len(args.output))

for path, sample in zip(args.output, samples):
    with open(path, 'w') as output_file:
        json.dump(sample, output_file)
