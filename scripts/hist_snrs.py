#!/usr/bin/env python3

import argparse
import json
import math

parser = argparse.ArgumentParser(description='Plot a histogram of SNR')
parser.add_argument('-t', '--target', default=None, type=float,
                    help='Plot target number of events')
parser.add_argument('-r', '--range', required=True, nargs=2, type=float,
                    help='Range of SNRs to plot')
parser.add_argument('-s', '--symmetric', help='Symmetric interval to use for '
                    'upper and lower percentiles', default=68, type=float)
parser.add_argument('-R', '--rate', action='store_true',
                    help='Plot as a rate, not as a number of events')
parser.add_argument('-b', '--bins', type=int, default=10,
                    help='Number of bins')
parser.add_argument('-T', '--threshold', type=float, default=0.,
                    help='Threshold in the SNR^2 of the odd part of the ' \
                    'waveform')
parser.add_argument('-m', '--memories', nargs='+',
                    help='Memory SNRs to plot')
parser.add_argument('-n', '--networks', nargs='+',
                    help='Networks to plot')
parser.add_argument('-o', '--output', help='Output files', default=None,
                    nargs='+')
parser.add_argument('snrs', nargs='+',
                    help='Files containing events and SNRs')

args = parser.parse_args()

import numpy as np
import matplotlib.pyplot as plt

import gwforecasts.utils.misc as misc
import gwforecasts.utils.plot

nets = None
keys = None

bins = None
edges = None

max_T = 0

for snr_file in args.snrs:
    with open(snr_file, 'r') as f:
        events = json.load(f)

    if len(events) == 0:
        continue

    if nets is None:
        if args.networks is None:
            nets = events[0]['mem_snr2s']
        else:
            nets = args.networks

    if keys is None:
        if args.memories is None:
            keys = events[0]['mem_snr2s'][next(iter(nets))]
        else:
            keys = args.memories

    snrs = {net: {key: [] for key in keys} for net in nets}

    if bins is None:
        bins = {net: {key: [] for key in keys} for net in nets}

    for event in events:
        if max_T < event['T']:
            max_T = event['T']

        for net in snrs:
            for key in snrs[net]:
                if args.threshold > event['cut_snr2s'][net]['Waveform (odd)']:
                    snrs[net][key].append(0.)
                    continue

                snrs[net][key].append(math.sqrt(event['mem_snr2s'][net][key]))

    for net in snrs:
        for key in snrs[net]:
            y, edges = np.histogram(snrs[net][key], range=args.range,
                                    bins=args.bins)

            for snr in snrs[net][key]:
                if snr >= args.range[1]:
                    y[-1] += 1

            cumul_y = np.cumsum(y[::-1])[::-1]

            bins[net][key].append(cumul_y)

for i, key in enumerate(keys):
    plt.figure(key)

    color_cycler = plt.rcParams['axes.prop_cycle']()

    for j, net in enumerate(nets):
        median, _ = misc.quantile(bins[net][key], 1/2)
        lower, _ = misc.quantile(bins[net][key],
                                 (1 - args.symmetric*0.01)/2)
        upper, _ = misc.quantile(bins[net][key],
                                 (1 + args.symmetric*0.01)/2)

        if args.rate:
            median /= max_T
            lower /= max_T
            upper /= max_T

        centers = (edges[1:] + edges[:-1])/2
        width = edges[1] - edges[0]

        plt.bar(centers, median, width=width, label='{0}'.format(net),
                color=next(color_cycler)['color'])

        error_locs = centers - width/2 + width*(j + 1)/(len(nets) + 1)

        _, caps, _ = plt.errorbar(error_locs, median,
                                  yerr=(median - lower, upper - median),
                                  color='black', capsize=5, marker='x',
                                  linestyle='')

        # caps[0].set_markersize(0)

    ylabel = key.lower()[:4]
    if len(key) > 4:
        ylabel += '.'

    if args.rate:
        plt.ylabel('Median rate of events with '
                   + '$\\rho_{{\\rm {0}}} > \\rho$\n'.format(ylabel)
                   + '(yr$^{-1}$)')
    else:
        plt.ylabel('Median events with'
                   + ' $\\rho_{{\\rm {0}}} > \\rho$'.format(ylabel))

    plt.yscale('log')
    plt.xlabel('$\\rho$')
    plt.legend()

    if args.target:
        plt.axhline(y=args.target, linestyle='--',
                    color=next(color_cycler)['color'])

    if args.output is not None:
        plt.savefig(args.output[i])

if args.output is None:
    plt.show()

