#!/usr/bin/env python3

import argparse
import json
import math

parser = argparse.ArgumentParser(description='Split an SNR file by year')
parser.add_argument('-y', '--years', help='List of years to split at',
                    nargs='+', type=float, required=True)
parser.add_argument('-o', '--out', help='Output files', nargs='+',
                    required=True)
parser.add_argument('snr2', help='List of events with SNR^2s')

args = parser.parse_args()

with open(args.snr2, 'r') as snr2_file:
    events = json.load(snr2_file)

years = iter(args.years)
outputs = iter(args.out)

current_year = next(years)

last_year = 0.

current_events = []

for event in events:
    if event['T'] < current_year:
        event['T'] -= last_year
        current_events.append(event)
    else:
        last_year = current_year

        try:
            current_year = next(years)
        except StopIteration:
            current_year = math.inf

        with open(next(outputs), 'w') as out:
            json.dump(current_events, out)

        current_events = []

with open(next(outputs), 'w') as out:
    json.dump(current_events, out)
