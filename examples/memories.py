#!/usr/bin/env python3

import math

import numpy as np
import matplotlib.pyplot as plt

from gwforecasts.waveform_processor import WaveformProcessor
from gwforecasts.detectors.network import Network

# This is the main object for doing computations on a given waveform.  Since the
# maximum $l$ mode is a quantity which is relevant at many points in the
# computation, it is set here.
wp = WaveformProcessor(ell_max=5)

# This sets the method that will be used to generate the waveform (that is, the
# specific surrogate model or EOB method), as well as the units to use for the
# final results.
#
# See waveform/generator.py to see the various other options that can be passed
# to this method.
wp.set_generator('NRSur7dq4', units='mks')

# A reasonable set of assumptions for second-generation gravitational wave
# detectors.  Note that $f_{\rm samp} = 2 f_{\rm high}$.
f_low = 10
f_samp = 8192

# This represents a GW150914-like event (with no spin)
chi_1, chi_2 = [[0, 0, 0]]*2

wp.set_waveform(M=65, dist=410,
                f_samp=f_samp, f_low=0,
                q=1/0.8, chi_1=chi_1, chi_2=chi_2)

inc = 7*np.pi/9
phi_ref = 2*np.pi/3

# For simplicity, we set these extrinsic parameters to zero
alpha = 0
delta = 0
psi = 0
T = 0

# Compute two types of memory effects: "displacement" (the usual) and "spin".
#
# See memories/base.py for additional options that can be passed along to the
# memory generation code, and memories/ generally for other types of effects
# that can be computed.
wp.add_memory(memory_type='Displacement')
wp.add_memory(memory_type='Spin')

# This is for describing the even/odd decomposition of the waveform under the
# transformation described in Sec. II.C.1 of [Grant & Nichols, 2022]
# (arXiv:2210.16266)
phi_flipped = phi_ref + np.pi/2
inc_flipped = inc

# Internally, the waveforms are stored in terms of spin-weighted spherical
# harmonic modes, and in a dimensionless unit system.  This function returns the
# plus and cross polarizations at the detector, using whatever unit system is
# supplied above.
u, h_true = wp.time_domain(inc=inc, phi_ref=phi_ref)
_, h_flipped = wp.time_domain(inc=inc_flipped, phi_ref=phi_flipped)

# Replace the two _ variables with real variable names in order to plot the
# windowed waveforms
freqs, htildes, _, _ = wp.freq_domain(inc=inc, phi_ref=phi_ref,
                                      # Determines size of windowing region
                                      window=1, pad=(1, 1),
                                      # (size is in seconds, assuming mks)
                                      fraction=False,
                                      return_windowed=True)

# Build a network based on location parameters (which influence the antenna
# patterns) and a given power spectral density in the noise.
#
# See the code in detectors/detector.py for additional options for building each
# detector in a network.
net = Network(network_config={
    "Hanford": {
        "detectors": [{
            "antenna_pattern": "LAL",
            "antenna_kwargs": {"detector": "H1"},
            "noise_psd": "LIGO-T2000012-v2",
            "noise_kwargs": {"detector": "LIGO", "run": "O4"}
        }]
    },
    "Livingston": {
        "detectors": [{
            "antenna_pattern": "LAL",
            "antenna_kwargs": {"detector": "L1"},
            "noise_psd": "LIGO-T2000012-v2",
            "noise_kwargs": {"detector": "LIGO", "run": "O4"}
        }]
    },
    "Virgo": {
        "detectors": [{
            "antenna_pattern": "LAL",
            "antenna_kwargs": {"detector": "V1"},
            "noise_psd": "LIGO-T2000012-v2",
            "noise_kwargs": {"detector": "Virgo", "run": "O4"}
        }]
    }
})

# Note: if $\alpha$, $\delta$, $\psi$, and $T$ are arrays, then this will return
# an *array* of pre-computed antenna patterns, for convenience
antennas = net.load_antenna(alpha, delta, psi, T)

# Each pair of plus and cross waveforms are stored by name in the return values
# of WaveformProcessor.time_domain (h_true and h_flipped) and
# WaveformProcessor.freq_domain (htildes)
for key in ('Displacement', 'Spin', 'Waveform'):
    snr2 = net.snr2(freqs, htildes[key], f_low, f_samp/2, antennas, T)
    print("{0}: {1}".format(key, math.sqrt(snr2)))

    plt.figure(key)

    plt.plot(u, h_true[key][0], label='Plus')
    plt.plot(u, h_true[key][1], label='Cross')

    plt.legend()

    plt.figure(key + ' (even and odd)')

    plt.plot(u, (h_true[key][0] + h_flipped[key][0])/2, label='Odd (plus)')
    plt.plot(u, (h_true[key][0] - h_flipped[key][0])/2, label='Even (plus)')
    plt.plot(u, (h_true[key][1] + h_flipped[key][1])/2, label='Odd (cross)')
    plt.plot(u, (h_true[key][1] - h_flipped[key][1])/2, label='Even (cross)')

    plt.legend()

    plt.figure(key + ' (FFT)')

    plt.plot(freqs, np.abs(htildes[key][0]), label='Plus')
    plt.plot(freqs, np.abs(htildes[key][1]), label='Cross')

    plt.loglog()

    plt.legend()

plt.show()
