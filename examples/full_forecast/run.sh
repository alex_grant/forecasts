#!/bin/bash

set -e

BASEDIR=$(realpath $(dirname $0))

SCRIPTS=${BASEDIR}/../../scripts

JOBS=4

# This generates events from a given population--for ease of example, we'll
# assume a fixed population

# mkdir -p ${BASEDIR}/pops ${BASEDIR}/events

# ${SCRIPTS}/generate_pops.py -m C -g 2 -r -z 0.3 \
#           -j ${JOBS} o1o2o3_mass_c_iid_mag_two_comp_iid_tilt_powerlaw_redshift_result.json \
#           $(seq -f ${BASEDIR}/pops/%1.f.json 0 9)

# ${SCRIPTS}/generate_events.py -t 1 -d -j ${JOBS} -c Planck15 \
#           -p ${BASEDIR}/pops ${BASEDIR}/events

mkdir -p ${BASEDIR}/events ${BASEDIR}/snrs

# Generate 10 realizations of a year's worth of events from a given population
${SCRIPTS}/generate_events.py -j ${JOBS} -t 1 -c Planck15 \
          -p ${BASEDIR}/pop.json \
          $(seq -f ${BASEDIR}/events/%1.f.json 0 9)

# Go through each each realization, generating the SNR in the memory
for event in $(find -L ${BASEDIR}/events -type f); do
    time ${SCRIPTS}/process.py -j ${JOBS} ${BASEDIR}/config.json \
         ${event} ${BASEDIR}/snrs/$(basename ${event});
done

# Generate memory as a function of time, and provide means/confidence intervals
$SCRIPTS/summarize_events.py -o $BASEDIR/snrs.json $BASEDIR/snrs/*.json

# Plot, with a threshold that effectively extrapolates out to five years
$SCRIPTS/plot_snrs.py -t 1.34 -o snr_plot.pdf -- snrs.json
