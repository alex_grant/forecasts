#!/usr/bin/env python3

import math
import numpy as np
import matplotlib.pyplot as plt
import astropy.cosmology as cosmo

import gwforecasts.populations.event as event
import gwforecasts.populations.primary as primary
import gwforecasts.populations.mass_ratio as mass_ratio
import gwforecasts.populations.rate as rate
import gwforecasts.populations.angles as angles

# Build an object that can generate samples of events given various population
# models, along with their parameters.
#
# See the various samplers in populations/primary.py, populations/mass_ratio.py,
# populations/rate.py, and populations/spin.py for additional models and the
# meanings of all of the model parameters.
sampler = event.EventSampler(sampler_classes=(primary.Truncated,
                                              mass_ratio.Truncated,
                                              rate.PowerLaw,
                                              angles.Uniform),
                             # This controls parameters of the MCMC sampling
                             walkers=80, processes=8,
                             # Various parameters (such as distance) are
                             # cosmology-dependent
                             cosmology=cosmo.Planck18,
                             # Various calculations involving redshift cannot be
                             # done analytically, and so we precompute at a
                             # (reasonable) number of redshifts and interpolate
                             z_samples=1000,
                             **{"R_0": 64.9,
                                "kappa": 0.0,
                                "z_max": 2.3,
                                "alpha": 0.4,
                                "beta_q": 0,
                                "m_min": 5,
                                "m_max": 41.6})

print(sampler.draw(2))
